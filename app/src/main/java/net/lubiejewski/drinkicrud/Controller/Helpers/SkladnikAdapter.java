package net.lubiejewski.drinkicrud.Controller.Helpers;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.R;

import java.util.ArrayList;
import java.util.List;

public class SkladnikAdapter extends ArrayAdapter<Skladnik> implements Filterable{
    private int resource;
    private List<Skladnik> originalData;
    private List<Skladnik> filteredData;
    private Filter filter = new ItemFilter();

    public SkladnikAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Skladnik> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.originalData = objects;
        this.filteredData = objects;
    }

    public int getCount() {
        return filteredData.size();
    }

    public Skladnik getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @NonNull
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Skladnik skladnik = getItem(position);

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);

        }

        TextView name = (TextView) convertView.findViewById(R.id.skladnik_nazwa);
        TextView producent = (TextView) convertView.findViewById(R.id.skladnik_producent);
        TextView autor = (TextView) convertView.findViewById(R.id.propozycja_autor);
        if(skladnik!=null) {
            name.setText(skladnik.getNazwa());
            producent.setText(skladnik.getProducent());
            if (skladnik.getAutor() != null) autor.setText(skladnik.getAutor().getName());
            else autor.setVisibility(View.GONE);
        }

        return convertView;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            final List<Skladnik> list = originalData;
            int count = list.size();
            final ArrayList<Skladnik> filteredList = new ArrayList<>(count);
            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getNazwa();
                if (filterableString.toLowerCase().contains(filterString)) {
                    filteredList.add(list.get(i));
                }
            }

            results.values = filteredList;
            results.count = filteredList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Skladnik>) results.values;
            notifyDataSetChanged();
        }

    }
}
