package net.lubiejewski.drinkicrud.Controller.Drink;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.APIRequest;
import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.Controller.Helpers.MultipartRequest;
import net.lubiejewski.drinkicrud.Controller.Skladnik.SkladnikChoose;
import net.lubiejewski.drinkicrud.Model.Drink;
import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class DrinkModyfikuj extends AppCompatActivity {
    private String TAG = DrinkModyfikuj.class.getSimpleName();
    private String APIurl;
    private String DrinkSkladnikURL;
    String authToken;

    JSONObject obiekt;
    JSONObject skladnikiJSON[];
    JSONObject noweSkladnikiJSON[];
    Drink dane;

    int id;
    EditText nazwaText;
    EditText przepisText;
    EditText ilosci[];
    int iloscSkladnikow;
    int noweSkladniki;

    private HashMap<Integer, Skladnik> wybraneSkladnikiHashMap;
    private HashMap<Integer, Skladnik> pobraneSkladnikiHashMap;
    private ArrayList<Skladnik> wybraneSkladniki;
    private ArrayList<Skladnik> pobraneSkladniki;

    private HashMap<Integer, Integer> id_drinkSkladnik;
    private HashMap<Integer, String> pobraneIlosci;
    private HashMap<Integer, JSONObject> JSONHash;

    private ProgressDialog dialog;

    private File photoFile;
    private Uri photoUri;
    private String imageURLString;
    private String thumbnailURL;
    private int imageID = 0;
    private boolean photoModified = false;
    private boolean imageUploadProgress = true;
    private boolean deleteButtonClicked = false;
    private boolean photoExists = false;
    private boolean doNotDeleteFile = false;

    private final Context context = this;
    private final String twoHyphens = "--";
    private final String lineEnd = "\r\n";
    private final String boundary = "apiclient-" + System.currentTimeMillis();
    private final String mimeType = "multipart/form-data;boundary=" + boundary;
    private byte[] multipartBody;

    private ArrayList<Integer> doUsuniecia;
    private ArrayList<Integer> doModyfikacji;
    private ArrayList<Integer> doDodania;

    @SuppressLint({"UseSparseArrays", "SimpleDateFormat"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_formularz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        id = getIntent().getIntExtra("id_drink", 0);
        APIurl = getResources().getString(R.string.serwer) + getResources().getString(R.string.table_drink) + "/" + id;
        DrinkSkladnikURL = getResources().getString(R.string.serwer) + getResources().getString(R.string.table_drink_has_skladnik);
        nazwaText = (EditText) findViewById(R.id.add_nazwa_drink);
        przepisText = (EditText) findViewById(R.id.add_przepis);
        dane = new Drink();
        imageID = 0;

        try {
            File outputDir = this.getCacheDir();
            photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        new getOne().execute();

        dane.setId(id);
        dane.setNazwa(getIntent().getStringExtra("tytul"));
        dane.setPrzepis(getIntent().getStringExtra("przepis"));
        nazwaText.setText(dane.getNazwa(), TextView.BufferType.EDITABLE);
        przepisText.setText(dane.getPrzepis(), TextView.BufferType.EDITABLE);

        wybraneSkladniki = new ArrayList<>();
        pobraneSkladniki = new ArrayList<>();
        doUsuniecia = new ArrayList<>();
        doModyfikacji = new ArrayList<>();
        doDodania = new ArrayList<>();
        JSONHash = new HashMap<>();
        pobraneIlosci = new HashMap<>();
        wybraneSkladnikiHashMap = new HashMap<>();
        pobraneSkladnikiHashMap = new HashMap<>();

        // ---------------------------------------------------

        Button galleryButton = (Button) findViewById(R.id.add_image_intent);
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(DrinkModyfikuj.this, new String[] {android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                } else{
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    if(photoPickerIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(photoPickerIntent, 1);
                    }
                }
            }
        });

        galleryButton.setVisibility(View.VISIBLE);

        Button cameraButton = (Button) findViewById(R.id.capture_image_button);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DrinkModyfikuj.this, new String[] {android.Manifest.permission.CAMERA}, 2);
                } else{
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(DrinkModyfikuj.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                    } else {
                        if (photoCapture.resolveActivity(getPackageManager()) != null) {
                            try {
                                if(photoFile!=null) {
                                    photoFile.delete();
                                    File outputDir = context.getCacheDir();
                                    photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                                }
                                if (photoFile != null) {
                                    photoUri = Uri.fromFile(photoFile);
                                    photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                                    startActivityForResult(photoCapture, 2);
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) cameraButton.setVisibility(View.GONE);

        // ---------------------------------------------------

        Button wybierzSkladniki = (Button) findViewById(R.id.chooseButton);
        wybierzSkladniki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppController.getInstance(view.getContext()).setWybraneSkladniki(wybraneSkladniki);
                Intent choose = new Intent(view.getContext(), SkladnikChoose.class);
                startActivityForResult(choose, 3);
            }
        });

        Button usunObrazek = (Button) findViewById(R.id.button_delete_image);
        usunObrazek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageID!=0) {
                    deleteButtonClicked = true;
                    deletePhoto();
                } else {
                    LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                    layout.setVisibility(View.GONE);
                    photoExists = false;
                }
            }
        });

        Button rotateLeft = (Button) findViewById(R.id.button_rotate_left);
        rotateLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotatePhoto(-90);
            }
        });

        Button rotateRight = (Button) findViewById(R.id.button_rotate_right);
        rotateRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotatePhoto(90);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modyfikujDrinka();
            }
        });
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case 1:
                //OBRAZEK
                if(resultCode == RESULT_OK && imageReturnedIntent != null){
                    photoModified = true;
                    photoExists = true;
                    Uri imageUri = imageReturnedIntent.getData();

                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(imageUri,filePathColumn, null, null, null);
                    if(cursor!=null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();

                        new copyImage(picturePath).execute();
                        LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                        layout.setVisibility(View.VISIBLE);

                        ImageView thumbnailView = (ImageView) findViewById(R.id.uploadThumbnail);
                        thumbnailView.setImageURI(Uri.parse(picturePath));
                    }
                }
                break;
            case 2:
                //APARAT
                if(photoUri != null) {
                    Bitmap src = decodeFile(photoFile, 960, 540);
                    photoModified = true;
                    photoExists = true;

                    LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                    layout.setVisibility(View.VISIBLE);
                    ImageView thumbnailView = (ImageView) findViewById(R.id.uploadThumbnail);
                    thumbnailView.setImageBitmap(src);
                }
                break;
            case 3:
                //SKLADNIKI
                wybraneSkladniki = AppController.getInstance(this).getWybraneSkladniki();
                wyswietlSkladniki();
                break;
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                } else{
                    Toast.makeText(context, getString(R.string.file_read_permissions_error), Toast.LENGTH_SHORT).show();
                }
                return;
            case 2:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(DrinkModyfikuj.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                    } else {
                        if (photoCapture.resolveActivity(getPackageManager()) != null) {
                            try {
                                File outputDir = this.getCacheDir();
                                photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                                photoUri = Uri.fromFile(photoFile);
                                photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                                startActivityForResult(photoCapture, 2);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Toast.makeText(context, getString(R.string.camera_permissions_error), Toast.LENGTH_SHORT).show();
                }
                return;
            case 3:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (photoCapture.resolveActivity(getPackageManager()) != null) {
                        try {
                            File outputDir = this.getCacheDir();
                            photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                            photoUri = Uri.fromFile(photoFile);
                            photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                            startActivityForResult(photoCapture, 2);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(context, getString(R.string.file_save_permissions_error), Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void rotatePhoto(int degree){
        Bitmap src = decodeFile(photoFile, 960, 540);
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        if (src != null) {
            try {
                photoModified = true;
                src = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
                FileOutputStream fOut = new FileOutputStream(photoFile);
                src.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                ImageView thumbnailView = (ImageView) findViewById(R.id.uploadThumbnail);
                thumbnailView.setImageBitmap(src);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void deletePhoto() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            authToken = user.getToken(false).getResult().getToken();
            APIRequest request = new APIRequest(Request.Method.DELETE, getString(R.string.image_server) + "/" + imageID, authToken, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    Log.d(TAG, "Volley deletion Status code: " + response.statusCode);
                    if (deleteButtonClicked) {
                        Toast.makeText(getApplicationContext(), getString(R.string.successful_delete), Toast.LENGTH_SHORT).show();
                    }
                    LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                    layout.setVisibility(View.GONE);
                    imageID = 0;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (deleteButtonClicked) {
                        Toast.makeText(getApplicationContext(), getString(R.string.data_download_error) + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            AppController.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
    }

    private void uploadPhoto(){
        imageUploadProgress = false;
        Bitmap src = decodeFile(photoFile, 960, 540);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(src!=null) {
            src.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] imageData = baos.toByteArray();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            try {
                buildPart(dos, imageData, "image.jpeg");
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                multipartBody = bos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                authToken = "Bearer " + user.getToken(false).getResult().getToken();

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", authToken);

                MultipartRequest multipartRequest = new MultipartRequest(getString(R.string.image_server), headers, mimeType, multipartBody, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);
                            jsonObject = jsonObject.getJSONObject("response");
                            if (!jsonObject.getBoolean("success")) {
                                Log.d(TAG, jsonObject.getString("message"));
                                Toast.makeText(context, getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Rzeczywiście upload success");
                                imageURLString = jsonObject.getString("imgURL");
                                thumbnailURL = jsonObject.getString("thumbnail");
                                imageID = jsonObject.getInt("id");
                                photoFile.delete();
                                imageUploadProgress = true;
                            }
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();
                    }
                });

                AppController.getInstance(context).addToRequestQueue(multipartRequest);
            }
        }
    }

    private void buildPart(DataOutputStream dataOutputStream, byte[] fileData, String fileName) throws IOException {
        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"image\"; filename=\""
                + fileName + "\"" + lineEnd);
        dataOutputStream.writeBytes(lineEnd);

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        dataOutputStream.writeBytes(lineEnd);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(photoUri!=null) {
            outState.putString("photoUri", photoUri.toString());
        }
        if(imageURLString!=null){
            outState.putString("imageURLString", imageURLString);
        }
        if(thumbnailURL!=null){
            outState.putString("thumbnailURL", thumbnailURL);
        }
        if(photoExists){
            outState.putBoolean("photoExists", true);
            outState.putBoolean("photoModified", photoModified);
            outState.putInt("imageID", imageID);
            doNotDeleteFile = true;
            outState.putString("photoFile", photoFile.getAbsolutePath());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("photoUri")) {
                photoUri = Uri.parse(savedInstanceState.getString("photoUri"));
            }
            if (savedInstanceState.containsKey("imageURLString")) {
                imageURLString = savedInstanceState.getString("imageURLString");
            }
            if (savedInstanceState.containsKey("thumbnailURL")) {
                thumbnailURL = savedInstanceState.getString("thumbnailURL");
            }
            if (savedInstanceState.containsKey("photoExists")){
                photoExists = savedInstanceState.getBoolean("photoExists");
                photoModified = savedInstanceState.getBoolean("photoModified");
                imageID = savedInstanceState.getInt("imageID");
                //noinspection ConstantConditions
                photoFile = new File(savedInstanceState.getString("photoFile"));
                LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                layout.setVisibility(View.VISIBLE);
                ImageView thumbnail = (ImageView) findViewById(R.id.uploadThumbnail);
                Picasso.with(getApplicationContext()).load(photoFile).into(thumbnail);
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    public static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            int scale=1;
            while(o.outWidth/scale/2>= WIDTH && o.outHeight/scale/2>= HIGHT)
                scale*=2;
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException ignored) {}
        return null;
    }

    private void onTextChange(EditText text, int skladnikID){
        final int idSkladnik = skladnikID;
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                wybraneSkladnikiHashMap.get(idSkladnik).setIlosc(editable.toString());
            }
        });
    }

    private void onButtonClick(Button button, int skladnikID){
        final int idSkladnik = skladnikID;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout linearParent =  (LinearLayout) view.getParent().getParent();
                RelativeLayout linearChild = (RelativeLayout) view.getParent();
                linearParent.removeView(linearChild);
                wybraneSkladnikiHashMap.remove(idSkladnik);
                int index = 0;
                for(Skladnik skladnik : wybraneSkladniki){
                    if(skladnik.getId_skladnik()==idSkladnik){
                        index = wybraneSkladniki.indexOf(skladnik);
                    }
                }
                wybraneSkladniki.remove(index);
            }
        });
    }

    @SuppressLint("UseSparseArrays")
    private void wyswietlSkladniki(){
        wybraneSkladnikiHashMap = new HashMap<>();
        for(Skladnik skladnik : wybraneSkladniki){
            wybraneSkladnikiHashMap.put(skladnik.getId_skladnik(), skladnik);
            Log.d(TAG, "Dodano " + skladnik.getNazwa());
        }
        if(wybraneSkladniki!=null) {
            LinearLayout dodajSkladniki = (LinearLayout) findViewById(R.id.skladniki_drinka_formularz);
            dodajSkladniki.removeAllViews();
            iloscSkladnikow = wybraneSkladniki.size();
            Log.d(TAG, "Ilosc skladnikow: " + iloscSkladnikow);
            ilosci = new EditText[iloscSkladnikow];

            for(int i=0;i<iloscSkladnikow;i++){
                View linia = getLayoutInflater().inflate(R.layout.drink_formularz_skladnik_item, dodajSkladniki, false);
                Skladnik liniaSkladnik = wybraneSkladniki.get(i);

                TextView nazwa = (TextView) linia.findViewById(R.id.ingredient_name);
                nazwa.setText(liniaSkladnik.getNazwa());
                ilosci[i] = (EditText) linia.findViewById(R.id.ingredient_quantity);
                ilosci[i].setText(liniaSkladnik.getIlosc());
                onTextChange(ilosci[i], liniaSkladnik.getId_skladnik());
                TextView jednostka = (TextView) linia.findViewById(R.id.ingredient_unit);
                jednostka.setText(liniaSkladnik.getJednostka());
                Button przycisk = (Button) linia.findViewById(R.id.ingredient_delete);
                onButtonClick(przycisk, liniaSkladnik.getId_skladnik());

                dodajSkladniki.addView(linia);
            }
        }
    }

    private void modyfikujDrinka() {
        if(photoModified){
            if(imageID!=0) deletePhoto();
            if(photoFile!=null && photoExists) uploadPhoto();
        }

        obiekt = new JSONObject();
        skladnikiJSON = new JSONObject[iloscSkladnikow];
        noweSkladnikiJSON = new JSONObject[noweSkladniki];

        EditText nazwaText = (EditText) findViewById(R.id.add_nazwa_drink);
        EditText przepisText = (EditText) findViewById(R.id.add_przepis);

        try {
            obiekt.put("nazwa", nazwaText.getText().toString());
            obiekt.put("przepis", przepisText.getText().toString());
            if(imageID!=0) obiekt.put("file_id", imageID);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) obiekt.put("user_id", user.getUid());

            //sprawdzanie dotychczasowych składników
            for(Skladnik skladnik : pobraneSkladniki){
                if(wybraneSkladnikiHashMap.containsKey(skladnik.getId_skladnik())){
                    if(!skladnik.getIlosc().equals(pobraneIlosci.get(skladnik.getId_skladnik()))){
                        doModyfikacji.add(skladnik.getId_skladnik());
                    }
                }
                else doUsuniecia.add(skladnik.getId_skladnik());
            }

            //sprawdzanie nowych składników
            for(Skladnik skladnik: wybraneSkladniki){
                if(!pobraneSkladnikiHashMap.containsKey(skladnik.getId_skladnik())) doDodania.add(skladnik.getId_skladnik());
            }

            for(Integer i : doModyfikacji){
                JSONObject modyfikowany = new JSONObject();
                Skladnik skladnik = wybraneSkladnikiHashMap.get(i);
                modyfikowany.put("ingredient_id", skladnik.getId_skladnik());
                modyfikowany.put("ilosc", skladnik.getIlosc());
                JSONHash.put(i, modyfikowany);
            }

            for(Integer i : doDodania){
                JSONObject dodawany = new JSONObject();
                Skladnik skladnik = wybraneSkladnikiHashMap.get(i);
                dodawany.put("ingredient_id", skladnik.getId_skladnik());
                dodawany.put("ilosc", skladnik.getIlosc());
                JSONHash.put(i, dodawany);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new modyfikuj().execute();

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onDestroy() {
        if(photoFile.exists()){
            if(!doNotDeleteFile)
                photoFile.delete();
        }
        super.onDestroy();
    }

    private class getOne extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(DrinkModyfikuj.this);
            dialog.setMessage(getString(R.string.progress_dialog));
            dialog.show();
        }

        @SuppressLint("UseSparseArrays")
        @Override
        protected Void doInBackground(Void... voids) {
            HttpHandler sh = new HttpHandler();
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) {
                authToken = user.getToken(false).getResult().getToken();
                String jsonStr = sh.makeServiceCall(APIurl, "GET", authToken);

                if (jsonStr != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        JSONObject drinkJSON = jsonObj.getJSONObject("data");

                        if (!drinkJSON.isNull("file_id")) {
                            imageID = drinkJSON.getInt("file_id");
                            JSONObject file = drinkJSON.getJSONObject("file");
                            String imageURL = file.getString("path");
                            if (URLUtil.isValidUrl(imageURL)) {
                                dane.setObrazek(new URL(imageURL));
                                setThumbnail(dane.getObrazek().toString());
                            }
                        }
                        JSONArray skladnikiJSON = drinkJSON.getJSONArray("ingredients");

                        pobraneSkladniki = new ArrayList<>();
                        id_drinkSkladnik = new HashMap<>();
                        pobraneSkladnikiHashMap = new HashMap<>();

                        for (int i = 0; i < skladnikiJSON.length(); i++) {
                            JSONObject skladnikJSON = skladnikiJSON.getJSONObject(i);
                            JSONObject joinJSON = skladnikJSON.getJSONObject("_joinData");
                            Skladnik skladnik = new Skladnik();
                            skladnik.setId_skladnik(skladnikJSON.getInt("id"));
                            skladnik.setNazwa(skladnikJSON.getString("nazwa"));
                            skladnik.setProducent(skladnikJSON.getString("producent"));
                            skladnik.setIlosc(joinJSON.getString("ilosc"));
                            skladnik.setJednostka(skladnikJSON.getString("jednostka"));
                            skladnik.setWybrany(true);

                            wybraneSkladniki.add(skladnik);
                            pobraneSkladniki.add(skladnik);
                            pobraneIlosci.put(skladnik.getId_skladnik(), skladnik.getIlosc());
                            pobraneSkladnikiHashMap.put(skladnik.getId_skladnik(), skladnik);
                            id_drinkSkladnik.put(skladnik.getId_skladnik(), joinJSON.getInt("id"));
                        }
                    } catch (final JSONException e) {
                        Log.e(TAG, "Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        getString(R.string.data_download_error_2),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "Couldn't get json from server.");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            }
            return null;
        }

        private void setThumbnail(String url){
            try {
                URL imageURL = new URL(url);
                Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                FileOutputStream fOutStream = new FileOutputStream(photoFile);
                bitmap.compress(Bitmap.CompressFormat.PNG,50, fOutStream);
                fOutStream.flush();
                fOutStream.close();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                        layout.setVisibility(View.VISIBLE);
                        ImageView thumbnail = (ImageView) findViewById(R.id.uploadThumbnail);
                        Picasso.with(getApplicationContext()).load(photoFile).into(thumbnail);
                        photoExists = true;
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wyswietlSkladniki();
                }
            });
            if (dialog.isShowing()) dialog.dismiss();
        }
    }

    private class modyfikuj extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                //noinspection StatementWithEmptyBody
                while (!imageUploadProgress){
                }
                if(imageID!=0) obiekt.put("file_id", imageID);

                URL url = new URL(APIurl);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("PUT");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(obiekt.toString());
                writer.flush();
                writer.close();

                for(Integer i : doUsuniecia){
                    usunStareSkladniki(id_drinkSkladnik.get(i));
                }

                for(Integer i : doModyfikacji){
                    edytujStareSkladniki(id_drinkSkladnik.get(i), i);
                }

                for(Integer i: doDodania){
                    wstawNoweSkladniki(id, i);
                }

                if (conn.getResponseCode() == 201) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.successful_save),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private void wstawNoweSkladniki(int idDrink, int idObiekt) {
            try {
                JSONObject wysylany = JSONHash.get(idObiekt);
                wysylany.put("drink_id", idDrink);
                URL url = new URL(DrinkSkladnikURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(wysylany.toString());
                writer.flush();
                writer.close();
                Log.d(TAG, "HTTP Response code(skladnik): " + conn.getResponseCode() + ", ze strony: " + DrinkSkladnikURL);
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        }

        private void edytujStareSkladniki(int joinID, int objectID){
            try{
                String modyfikujURL = DrinkSkladnikURL + "/" + joinID;
                URL url = new URL(modyfikujURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("PUT");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(JSONHash.get(objectID).toString());
                writer.flush();
                writer.close();

                Log.d(TAG, "Update starego skladnika: response - " + conn.getResponseCode());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void usunStareSkladniki(int joinID){
            try {
                String usunURL = DrinkSkladnikURL + "/" + joinID;
                URL url = new URL(usunURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                conn.setRequestMethod("DELETE");
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Authorization",authToken);
                Log.d(TAG, "Usuwanie starego skladnika: joinID - " + joinID);
                Log.d(TAG, "Usuwanie starego skladnika: response - " + conn.getResponseCode());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class copyImage extends AsyncTask<Void,Void,Void> {
        private String sourcePath;

        copyImage(String source){
            this.sourcePath = source;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                File source = new File(sourcePath);
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(photoFile).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
