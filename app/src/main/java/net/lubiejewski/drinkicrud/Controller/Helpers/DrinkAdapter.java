package net.lubiejewski.drinkicrud.Controller.Helpers;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.lubiejewski.drinkicrud.Model.Drink;
import net.lubiejewski.drinkicrud.R;

import java.util.ArrayList;
import java.util.List;

public class DrinkAdapter extends ArrayAdapter<Drink> implements Filterable {
    private int resource;
    private List<Drink> originalData;
    private List<Drink> filteredData;
    private Filter filter = new ItemFilter();

    public DrinkAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Drink> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.originalData = objects;
        this.filteredData = objects;
    }

    public int getCount() {
        return filteredData.size();
    }

    public Drink getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @NonNull
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        Drink drink = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);
        }

        TextView nazwaDrinka = (TextView) convertView.findViewById(R.id.nazwa_drinka);
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);

        if(drink!=null) {
            nazwaDrinka.setText(drink.getNazwa());
            nazwaDrinka.setTag(drink.getObrazek());

            if (drink.getObrazek() != null)
                Picasso.with(getContext()).load(drink.getObrazek().toString())
                        .fit()
                        .centerCrop()
                        .into(thumbnail);
            else
                Picasso.with(getContext()).load(R.drawable.butelka).into(thumbnail);
        }
        return convertView;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Drink> list = originalData;

            int count = list.size();
            final ArrayList<Drink> filteredList = new ArrayList<>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getNazwa();
                if (filterableString.toLowerCase().contains(filterString)) {
                    filteredList.add(list.get(i));
                }
            }

            results.values = filteredList;
            results.count = filteredList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Drink>) results.values;
            notifyDataSetChanged();
        }

    }
}
