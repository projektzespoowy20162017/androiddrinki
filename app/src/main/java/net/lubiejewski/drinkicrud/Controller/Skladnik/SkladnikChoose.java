package net.lubiejewski.drinkicrud.Controller.Skladnik;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.Controller.Helpers.SkladnikExpandableAdapter;
import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.Model.SkladnikGroup;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SkladnikChoose extends AppCompatActivity {
    private String TAG = SkladnikChoose.class.getSimpleName();
    private ProgressDialog dialog;
    private String skladnikURL;

    ArrayList<SkladnikGroup> groups = new ArrayList<>();
    SkladnikGroup alkoholeGroup;
    SkladnikGroup dodatkiGroup;
    ExpandableListView expandable;
    ArrayList<Skladnik> alkohole;
    ArrayList<Skladnik> dodatki;
    public ArrayList<Skladnik> wybraneSkladniki;

    private SkladnikExpandableAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skladnik_choose);

        skladnikURL = getString(R.string.serwer) + getString(R.string.table_skladnik) + "?sort=nazwa";

        wybraneSkladniki = AppController.getInstance(this).getWybraneSkladniki();

        expandable = (ExpandableListView) findViewById(R.id.skladnik_expandable);

        alkoholeGroup = new SkladnikGroup();
        dodatkiGroup = new SkladnikGroup();
        alkoholeGroup.setName(getString(R.string.choose_alcohol));
        dodatkiGroup.setName(getString(R.string.choose_nonalcohol));

        new getList().execute();

        expandable.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPos, int itemPos, long id) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.skladnik_choose_checkbox);
                Skladnik checked = (Skladnik) expandableListView.getExpandableListAdapter().getChild(groupPos, itemPos);
                if(checkBox.isChecked()) {
                    wybraneSkladniki.remove(checked);
                    checkBox.setChecked(false);
                } else{
                    wybraneSkladniki.add(checked);
                    checkBox.setChecked(true);
                }
                return false;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zwrocListe();
            }
        });
    }

    private void expandAll() {
        int count = adapter.getGroupCount();
        for (int i = 0; i < count; i++){
            expandable.expandGroup(i);
        }
    }

    private void zwrocListe(){
        AppController.getInstance(this).setWybraneSkladniki(wybraneSkladniki);
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private class getList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(SkladnikChoose.this);
            dialog.setMessage(getString(R.string.progress_dialog));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) {
                HttpHandler handler = new HttpHandler();
                String authToken = "Bearer " + user.getToken(false).getResult().getToken();
                String jsonStr = handler.makeServiceCall(skladnikURL, "GET", authToken);

                alkohole = new ArrayList<>();
                dodatki = new ArrayList<>();

                if (jsonStr != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        JSONArray skladniki = jsonObj.getJSONArray("ingredients");

                        for (int i = 0; i < skladniki.length(); i++) {
                            JSONObject skladnikJSON = skladniki.getJSONObject(i);

                            Skladnik skladnik = new Skladnik();
                            skladnik.setId_skladnik(skladnikJSON.getInt("id"));
                            skladnik.setNazwa(skladnikJSON.getString("nazwa"));
                            skladnik.setAlkohol(skladnikJSON.getBoolean("alkohol"));
                            skladnik.setJednostka(skladnikJSON.getString("jednostka"));
                            skladnik.setProducent(skladnikJSON.getString("producent"));
                            if (wybraneSkladniki.contains(skladnik)) {
                                skladnik.setWybrany(true);
                            } else {
                                skladnik.setWybrany(false);
                            }
                            if (skladnik.isAlkohol()) {
                                alkohole.add(skladnik);
                            } else {
                                dodatki.add(skladnik);
                            }
                        }
                    } catch (final JSONException e) {
                        Log.e(TAG, "Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SkladnikChoose.this,
                                        getString(R.string.data_download_error_2),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });

                    }
                } else {
                    Log.e(TAG, "Couldn't get json from server.");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SkladnikChoose.this,
                                    getString(R.string.data_download_error),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (dialog.isShowing()) dialog.dismiss();

            alkoholeGroup.setSkladnikList(alkohole);
            dodatkiGroup.setSkladnikList(dodatki);

            groups = new ArrayList<>();
            groups.add(alkoholeGroup);
            groups.add(dodatkiGroup);

            adapter = new SkladnikExpandableAdapter(SkladnikChoose.this, groups);
            expandable.setAdapter(adapter);
            expandAll();
        }
    }
}
