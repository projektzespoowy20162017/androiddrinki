package net.lubiejewski.drinkicrud.Controller.Skladnik;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class SkladnikDodaj extends AppCompatActivity {
    String APIurl;
    String authToken;

    JSONObject obiekt;

    private boolean propositionsFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skladnik_formularz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        propositionsFlag = getIntent().getBooleanExtra("mode", false);

        APIurl = getString(R.string.serwer) + getString(R.string.table_skladnik);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dodajSkladnik();
            }
        });
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void dodajSkladnik(){
        obiekt = new JSONObject();

        EditText nazwa = (EditText) findViewById(R.id.add_nazwa_skladnik);
        EditText producent = (EditText) findViewById(R.id.add_producent);
        EditText jednostka = (EditText) findViewById(R.id.add_jednostka);
        Switch alkohol = (Switch) findViewById(R.id.alkohol_switch);

        try {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) if(propositionsFlag) obiekt.put("user_id", user.getUid());
            obiekt.put("nazwa", nazwa.getText().toString());
            obiekt.put("producent", producent.getText().toString());
            obiekt.put("jednostka",jednostka.getText().toString());
            if(propositionsFlag) obiekt.put("status", false);
            else obiekt.put("status", true);
            if(alkohol.isChecked()) obiekt.put("alkohol",true);
            else obiekt.put("alkohol",false);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new dodaj().execute();

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private class dodaj extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null) {
                    URL url = new URL(APIurl);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                    authToken = "Bearer " + user.getToken(false).getResult().getToken();

                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Authorization", authToken);

                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(obiekt.toString());
                    writer.flush();
                    writer.close();

                    if (conn.getResponseCode() == 201) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        getString(R.string.successful_save),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
