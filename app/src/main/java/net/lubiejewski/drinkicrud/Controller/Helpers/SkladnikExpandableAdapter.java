package net.lubiejewski.drinkicrud.Controller.Helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.Model.SkladnikGroup;
import net.lubiejewski.drinkicrud.R;

import java.util.ArrayList;

import static android.R.id.list;

public class SkladnikExpandableAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<SkladnikGroup> groups;

    public SkladnikExpandableAdapter(Context context, ArrayList<SkladnikGroup> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPos) {
        return groups.get(groupPos).getSkladnikList().size();
    }

    @Override
    public Object getGroup(int groupPos) {
        return groups.get(groupPos);
    }

    @Override
    public Object getChild(int groupPos, int itemPos) {
        return groups.get(groupPos).getSkladnikList().get(itemPos);
    }

    @Override
    public long getGroupId(int groupPos) {
        return groupPos;
    }

    @Override
    public long getChildId(int groupPos, int itemPos) {
        return itemPos;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPos, boolean isLast, View view, ViewGroup viewGroup) {
        SkladnikGroup group = (SkladnikGroup) getGroup(groupPos);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.skladnik_choose_group, null);
        }

        TextView header = (TextView) view.findViewById(R.id.skladnik_expandable_group);
        header.setText(group.getName().trim());

        return view;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(final int groupPos, final int itemPos, boolean isLast, View view, ViewGroup viewGroup) {
        Skladnik skladnik = (Skladnik) getChild(groupPos, itemPos);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.skladnik_choose_item, null);
        }

        CheckBox box = (CheckBox) view.findViewById(R.id.skladnik_choose_checkbox);
        box.setText(skladnik.getNazwa().trim());
        box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                groups.get(groupPos).getSkladnikList().get(itemPos).setWybrany(buttonView.isChecked());
            }
        });
        if(skladnik.isWybrany()) box.setChecked(true);
        else box.setChecked(false);

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

}
