package net.lubiejewski.drinkicrud.Controller.Helpers;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.util.HashMap;
import java.util.Map;

public class APIRequest extends Request<NetworkResponse> {
    private final Response.Listener<NetworkResponse> listener;
    private final String token;

    public APIRequest(int method, String url, String token, Response.Listener<NetworkResponse> listener, Response.ErrorListener errorListener){
        super(method, url, errorListener);
        this.token = token;
        this.listener = listener;
    }

    @Override
    protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            return Response.success(
                    response,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(NetworkResponse response) {
        listener.onResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        if(token!=null) headers.put("Authorization", "Bearer " + token);
        return headers;
    }
}
