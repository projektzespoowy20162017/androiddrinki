package net.lubiejewski.drinkicrud.Controller.Helpers;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();

    public HttpHandler() {
    }

    public String getToken(String username, String password, String tokenURL){
        String token = null;
        try{
            URL url = new URL(tokenURL);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Content-Type","application/json");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            JSONObject obiekt = new JSONObject();
            obiekt.put("username",username);
            obiekt.put("password",password);

            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(obiekt.toString());
            writer.flush();
            writer.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            String jsonStr = convertStreamToString(in);
            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject data = jsonObj.getJSONObject("data");
            token = data.getString("token");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return "Bearer " + token;
    }

    public String getDataNoToken(String url){
        String response = null;
        try{
            URL urlObj = new URL(url);
            HttpsURLConnection conn = (HttpsURLConnection) urlObj.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept","application/json");
            Log.i("Connection", "HTTP Response code : " + conn.getResponseCode());
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        return response;
    }

    public String makeServiceCall(String reqUrl,String method, String authToken) {
        String response = null;
        try {
            URL url = new URL(reqUrl);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod(method);
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization",authToken);
            Log.i("Connection", "HTTP Response code : " + conn.getResponseCode());
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
            //conn.disconnect();
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}