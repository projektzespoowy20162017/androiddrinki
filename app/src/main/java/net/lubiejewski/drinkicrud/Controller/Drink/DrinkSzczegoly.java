package net.lubiejewski.drinkicrud.Controller.Drink;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.MainActivity;
import net.lubiejewski.drinkicrud.Model.Drink;
import net.lubiejewski.drinkicrud.Model.Komentarz;
import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.Model.User;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class DrinkSzczegoly extends AppCompatActivity {
    private String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog dialog;
    String APIurl;
    String anonView;
    String UserUrl;
    String KomentarzUrl;
    //String DrinkKomentarzURL;
    String OcenaUrl;
    //String DrinkOcenaURL;
    private String authToken;

    JSONObject komentarzWysylany;
    JSONObject ratingWysylany;
    HttpHandler sh;

    private Drink dane;
    private User autor;
    private boolean rated;
    private float userRating;
    private int userRatingID;

    final private String defaultImageURL = "https://images.pexels.com/photos/160150/pexels-photo-160150.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_szczegoly);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dane = new Drink();
        dane.setId(getIntent().getIntExtra("id_drink",0));
        dane.setNazwa(getIntent().getStringExtra("tytul"));
        dane.setPrzepis(getIntent().getStringExtra("przepis"));
        dane.setOcena(getIntent().getFloatExtra("ocena",0));

        APIurl = getString(R.string.serwer) + getString(R.string.table_drink) + "/" + dane.getId();
        anonView = getString(R.string.anonymous_drink_view) + "/" + dane.getId();
        UserUrl = getString(R.string.serwer) + getString(R.string.table_uzytkownik) + "/";
        KomentarzUrl = getString(R.string.serwer) + getString(R.string.table_komentarz);
        //DrinkKomentarzURL = getString(R.string.serwer) + getString(R.string.table_drink_has_komentarz);
        OcenaUrl = getString(R.string.serwer) + getString(R.string.table_ocena);
        //DrinkOcenaURL = getString(R.string.serwer) + getString(R.string.table_drink_has_ocena);

        setTitle(dane.getNazwa());
        TextView przepis = (TextView) findViewById(R.id.drink_szczegoly_przepis);
        przepis.setText(dane.getPrzepis());
        RatingBar ratingGlobal = (RatingBar) findViewById(R.id.ratingGlobal);
        ratingGlobal.setRating(dane.getOcena());

        new getOne().execute();

        Button dodajKomentarz = (Button) findViewById(R.id.dodaj_komentarz);
        dodajKomentarz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wyslijKomentarz();
            }
        });

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingUser);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float value, boolean fromUser) {
                if(fromUser) ocen(value);
            }
        });

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FirebaseAuth.AuthStateListener listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                LinearLayout dodajKomentarz = (LinearLayout) findViewById(R.id.drink_szczegoly_komentuj);
                LinearLayout dodajOcene = (LinearLayout) findViewById(R.id.drink_szczegoly_ocen);
                LinearLayout anonMessage = (LinearLayout) findViewById(R.id.drink_szczegoly_anonymous);
                if(user!=null){
                    dodajKomentarz.setVisibility(View.VISIBLE);
                    dodajOcene.setVisibility(View.VISIBLE);
                    anonMessage.setVisibility(View.GONE);
                }
                else{
                    dodajKomentarz.setVisibility(View.GONE);
                    dodajOcene.setVisibility(View.GONE);
                    anonMessage.setVisibility(View.VISIBLE);
                }
            }
        };
        FirebaseAuth.getInstance().addAuthStateListener(listener);
    }

    private void wyswietlSkladniki(){
        LinearLayout skladnikiLayout = (LinearLayout) findViewById(R.id.szczegoly_skladniki);
        skladnikiLayout.removeAllViews();
        Toolbar.LayoutParams params = new Toolbar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        List<Skladnik> skladniki = dane.getSkladniki();
        Log.d(TAG, "Ilosc skladnikow: " + skladniki.size());
        for (Skladnik skladnik : skladniki) {
            LinearLayout linia = new LinearLayout(DrinkSzczegoly.this);
            linia.setLayoutParams(params);
            linia.setOrientation(LinearLayout.HORIZONTAL);

            CheckBox checkBox = new CheckBox(DrinkSzczegoly.this);          //do listy zakupow
            checkBox.setLayoutParams(params);

            TextView skladnikText = new TextView(DrinkSzczegoly.this);
            skladnikText.setLayoutParams(params);
            skladnikText.setText(skladnik.getNazwa());

            TextView ilosc = new TextView(DrinkSzczegoly.this);
            ilosc.setPadding(5, 0, 0, 0);
            ilosc.setLayoutParams(params);
            ilosc.setText(" - " + skladnik.getIlosc() + skladnik.getJednostka());

            linia.addView(checkBox);
            linia.addView(skladnikText);
            linia.addView(ilosc);
            skladnikiLayout.addView(linia);
        }
    }

    @SuppressLint("SimpleDateFormat")
    private void wyswietlKomentarze(){
        LinearLayout komentarzeLayout = (LinearLayout) findViewById(R.id.szczegoly_komentarze);
        komentarzeLayout.removeAllViews();
        List<Komentarz> komentarze = dane.getKomentarze();
        Log.d(TAG, "Ilosc komentarzy: " + komentarze.size());
        for (Komentarz komentarz : komentarze) {
            View linia = getLayoutInflater().inflate(R.layout.comment, komentarzeLayout, false);

            TextView komentarzAutor = (TextView) linia.findViewById(R.id.comment_user);
            komentarzAutor.setText(komentarz.getUser().getName());

            TextView komentarzData = (TextView) linia.findViewById(R.id.comment_date);
            SimpleDateFormat getFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            SimpleDateFormat printFormat = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date date = getFormat.parse(komentarz.getDataDodania());
                komentarzData.setText(printFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            TextView komentarzTresc = (TextView) linia.findViewById(R.id.comment_content);
            komentarzTresc.setText(komentarz.getKomentarz());

            if (komentarz.getUser().getObrazek() != null) {
                ImageView imageView = (ImageView) linia.findViewById(R.id.comment_avatar);
                Picasso.with(DrinkSzczegoly.this).load(komentarz.getUser().getObrazek().toString())
                        .fit()
                        .centerCrop()
                        .into(imageView);
            }

            komentarzeLayout.addView(linia);

        }
    }

    private void wyslijKomentarz(){
        EditText komentarzPole = (EditText) findViewById(R.id.komentarz_pole);
        komentarzWysylany = new JSONObject();
        try{
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) {
                komentarzWysylany.put("user_id", user.getUid());
                komentarzWysylany.put("drink_id", dane.getId());
                komentarzWysylany.put("komentarz", komentarzPole.getText().toString());
                new sendComment().execute();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.comment_send_error), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ocen(float ocena){
        ratingWysylany = new JSONObject();
        try{
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) {
                ratingWysylany.put("user_id", user.getUid());
                ratingWysylany.put("drink_id", dane.getId());
                ratingWysylany.put("ocena",ocena);
                new rateDrink().execute();
                new getOne().execute();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.rating_send_error), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class getOne extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(DrinkSzczegoly.this);
            dialog.setMessage(getString(R.string.progress_dialog));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            sh = new HttpHandler();
            String jsonStr;
            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
            if(currentUser!=null){
                authToken = "Bearer " + currentUser.getToken(false).getResult().getToken();
                jsonStr = sh.makeServiceCall(APIurl,"GET",authToken);
            }
            else jsonStr = sh.getDataNoToken(anonView);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONObject drinkJSON = jsonObj.getJSONObject("data");

                    JSONArray skladnikiJSON = drinkJSON.getJSONArray("ingredients");
                    JSONArray komentarzeJSON = drinkJSON.getJSONArray("comments");
                    JSONObject autorJSON = drinkJSON.getJSONObject("user");
                    dane.setOcena((float) drinkJSON.getDouble("average"));
                    dane.setPrzepis(drinkJSON.getString("przepis"));

                    autor = new User();
                    autor.setID(autorJSON.getString("id"));
                    autor.setName(autorJSON.getString("username"));
                    autor.setActive(autorJSON.getBoolean("active"));
                    autor.setAdmin(autorJSON.getBoolean("isAdmin"));

                    List<Skladnik> skladniki = new ArrayList<>();
                    List<Komentarz> komentarze = new ArrayList<>();

                    userRatingID = jsonObj.getInt("ratingID");
                    rated = userRatingID != 0;
                    Log.d(TAG, "ratingID: " + userRatingID);
                    if(rated) userRating = getUserRating(userRatingID);

                    if(!drinkJSON.isNull("file_id")) {
                        JSONObject file = drinkJSON.getJSONObject("file");
                        String imageURL = file.getString("path");
                        if(URLUtil.isValidUrl(imageURL))
                            setImageURL(imageURL);
                    }

                    for (int i = 0; i < skladnikiJSON.length(); i++) {
                        JSONObject skladnikJSON = skladnikiJSON.getJSONObject(i);
                        JSONObject joinJSON = skladnikJSON.getJSONObject("_joinData");
                        Skladnik skladnik = new Skladnik();
                        skladnik.setId_skladnik(skladnikJSON.getInt("id"));
                        skladnik.setNazwa(skladnikJSON.getString("nazwa"));
                        skladnik.setProducent(skladnikJSON.getString("producent"));
                        skladnik.setIlosc(joinJSON.getString("ilosc"));
                        skladnik.setJednostka(skladnikJSON.getString("jednostka"));
                        skladnik.setAlkohol(skladnikJSON.getBoolean("alkohol"));

                        skladniki.add(skladnik);
                    }

                    Log.d(TAG, "Ilosc skladnikow w getOne: " + skladniki.size());

                    for (int i = 0; i < komentarzeJSON.length(); i++) {
                        JSONObject komentarzJSON = komentarzeJSON.getJSONObject(i);
                        Komentarz komentarz = new Komentarz();
                        komentarz.setId(komentarzJSON.getInt("id"));
                        komentarz.setKomentarz(komentarzJSON.getString("komentarz"));
                        komentarz.setDataDodania(komentarzJSON.getString("created"));
                        komentarz.setDataModyfikacji(komentarzJSON.getString("modified"));

                        JSONObject userJSON = komentarzJSON.getJSONObject("user");
                        User user = new User();
                        user.setID(komentarzJSON.getString("user_id"));
                        if(userJSON.isNull("name")) {
                            user.setName(userJSON.getString("username"));
                        } else{
                            user.setName(userJSON.getString("name"));
                        }
                        user.setActive(userJSON.getBoolean("active"));
                        user.setAdmin(userJSON.getBoolean("isAdmin"));

                        if(!userJSON.isNull("file_id")){
                            int file_id=userJSON.getInt("file_id");
                            user.setObrazekID(file_id);
                            JSONArray pliki = userJSON.getJSONArray("files");
                            for(int j=0; j<pliki.length(); j++){
                                JSONObject plik = pliki.getJSONObject(j);
                                if(plik.getInt("id")==file_id){
                                    user.setObrazek(new URL(plik.getString("thumbPath")));
                                }
                            }
                        }

                        komentarz.setUser(user);
                        komentarze.add(komentarz);
                    }

                    dane.setSkladniki(skladniki);
                    dane.setKomentarze(komentarze);


                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.data_download_error),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        private float getUserRating(int id){
            float rating = 0;
            String jsonStr = sh.makeServiceCall(OcenaUrl + "/" + id,"GET",authToken);
            if(jsonStr != null){
                try{
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONObject data = jsonObj.getJSONObject("rating");
                    rating = (float) data.getDouble("ocena");
                    Log.i(TAG, "Ocena użytkownika: " + rating);
                }
                catch (final JSONException e){
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
            return rating;
        }

        private void setImageURL(String url){
            if(url!=null) try {
                dane.setObrazek(new URL(url));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    wyswietlSkladniki();
                    wyswietlKomentarze();
                    RatingBar ratingGlobal = (RatingBar) findViewById(R.id.ratingGlobal);
                    ratingGlobal.setRating(dane.getOcena());
                    RatingBar ratingUser = (RatingBar) findViewById(R.id.ratingUser);
                    ratingUser.setRating(userRating);
                    TextView autorText = (TextView) findViewById(R.id.drink_szczegoly_autor);
                    autorText.setText(autor.getName());
                    TextView przepis = (TextView) findViewById(R.id.drink_szczegoly_przepis);
                    przepis.setText(dane.getPrzepis());

                    ImageView imageView = (ImageView) findViewById(R.id.image);
                    if(dane.getObrazek()!=null) {
                        if (URLUtil.isValidUrl(dane.getObrazek().toString())) {
                            Picasso.with(DrinkSzczegoly.this).load(dane.getObrazek().toString())
                                    .into(imageView);
                        } else {
                            Picasso.with(DrinkSzczegoly.this).load(defaultImageURL)
                                    .into(imageView);
                        }
                    } else{
                        Picasso.with(DrinkSzczegoly.this).load(defaultImageURL)
                                .into(imageView);
                    }
                }
            });
            if (dialog.isShowing()) dialog.dismiss();
        }
    }

    private class sendComment extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(KomentarzUrl);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                Log.i(TAG, "Link do komentarzy: " + KomentarzUrl);
                Log.i(TAG, "Dodawany komentarz: " + komentarzWysylany.toString());

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(komentarzWysylany.toString());
                writer.flush();
                writer.close();

                pobierzID(conn);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        private int pobierzID(HttpsURLConnection conn) throws IOException {
            int id = 0;
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String response = sh.convertStreamToString(in);
            if(response != null){
                try{
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject data = jsonObj.getJSONObject("data");
                    id = data.getInt("id");
                    Log.i(TAG, "ID dodanego komentarza: " + id);
                }
                catch (final JSONException e){
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
            return id;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    EditText komentarzPole = (EditText) findViewById(R.id.komentarz_pole);
                    komentarzPole.setText("");
                    new getOne().execute();
                }
            });
        }
    }

    private class rateDrink extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            if(rated) update();
            else add();
            return null;
        }

        private void add(){
            try {
                URL url = new URL(OcenaUrl);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(ratingWysylany.toString());
                writer.flush();
                writer.close();

                Log.d(TAG, "Dodawanie oceny - response: " + conn.getResponseCode());

                //int ratingID = pobierzID(conn);

                //dodajPowiazanie(ratingID);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void update(){
            try {
                URL url = new URL(OcenaUrl+ "/" + userRatingID);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("PUT");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(ratingWysylany.toString());
                writer.flush();
                writer.close();

                Log.d(TAG, "Aktualizowanie oceny - response: " + conn.getResponseCode());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /*
        private int pobierzID(HttpsURLConnection conn) throws IOException {
            int id = 0;
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String response = sh.convertStreamToString(in);
            if(response != null){
                try{
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject data = jsonObj.getJSONObject("data");
                    id = data.getInt("id");
                    Log.i(TAG, "ID dodanej oceny: " + id);
                }
                catch (final JSONException e){
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
            return id;
        }
        */
        /*
        private void dodajPowiazanie(int ratingID){
            try {
                JSONObject powiazanie = new JSONObject();
                powiazanie.put("drink_id",dane.getId());
                powiazanie.put("rating_id",ratingID);

                URL url = new URL(DrinkOcenaURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Authorization", authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(powiazanie.toString());
                writer.flush();
                writer.close();
                Log.d(TAG, "HTTP Response code(ocena): " + conn.getResponseCode() + ", ze strony: " + DrinkOcenaURL);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        */
    }
}
