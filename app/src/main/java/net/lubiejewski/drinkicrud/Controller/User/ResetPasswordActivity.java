package net.lubiejewski.drinkicrud.Controller.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import net.lubiejewski.drinkicrud.R;

import static net.lubiejewski.drinkicrud.R.layout.activity_reset_password;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText emailEt;
    private FirebaseAuth auth;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(activity_reset_password);
        emailEt = (EditText)findViewById(R.id.email_address);
        findViewById(R.id.reset_pass).setOnClickListener(this);
        auth = FirebaseAuth.getInstance();

    }

    private void sendResetEmail(){
        String email = emailEt.getText().toString().trim();

        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(ResetPasswordActivity.this, getString(R.string.password_reset_success), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResetPasswordActivity.this, getString(R.string.password_reset_fail), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        Intent intent = new Intent(ResetPasswordActivity.this, EmailPasswordActivity.class);
        intent.putExtra("email",email);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if(i == R.id.reset_pass){
            sendResetEmail();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
