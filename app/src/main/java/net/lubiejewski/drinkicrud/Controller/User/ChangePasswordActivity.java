package net.lubiejewski.drinkicrud.Controller.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.MainActivity;
import net.lubiejewski.drinkicrud.R;

import static net.lubiejewski.drinkicrud.R.layout.change_pass;

public class ChangePasswordActivity extends EmailPasswordActivity implements View.OnClickListener {
    private static final String TAG = "ChanginPassword";
    TextView  textView;
    EditText oldP;
    EditText newP;
    EditText confP;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(change_pass);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = null;
        if(user!=null) email = user.getEmail();
        findViewById(R.id.change_pass_button).setOnClickListener(this);
        textView = (TextView) findViewById(R.id.change_pass_tv);
        oldP = (EditText)findViewById(R.id.old_pass);
        newP = (EditText)findViewById(R.id.new_pass);
        confP = (EditText)findViewById(R.id.confirm_new_pass);
        textView.setText(email);
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v){
        int i = v.getId();
        if(i == R.id.change_pass_button){
            TextView oldPass = (TextView)findViewById(R.id.old_pass);
            TextView newPass = (TextView)findViewById(R.id.new_pass);
            String oldPassword = oldPass.getText().toString();
            final String newPassword = newPass.getText().toString();
            textView = (TextView) findViewById(R.id.change_pass_tv);
            if(changePasswordValidateForm()) {
                change_password(newPassword, oldPassword);
            }
        }
    }
    private void change_password(final String newPassword, String oldPassword){
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null) {
            String email = user.getEmail();
            if(email!=null) {
                AuthCredential credential = EmailAuthProvider.getCredential(email, oldPassword);
                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            user.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "Password updated");
                                        Toast.makeText(ChangePasswordActivity.this, getString(R.string.password_change_success), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Log.d(TAG, "Error password not updated");
                                        Toast.makeText(ChangePasswordActivity.this, getString(R.string.password_change_fail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            Log.d(TAG, "Error auth failed");
                            Toast.makeText(ChangePasswordActivity.this, getString(R.string.password_change_incorrect), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
    }

    private boolean changePasswordValidateForm() {
        boolean valid = true;

        String oldPass = oldP.getText().toString();
        if (TextUtils.isEmpty(oldPass)) {
            oldP.setError("Required.");
            valid = false;
        } else {
            oldP.setError(null);
        }

        String password = newP.getText().toString();
        String password2 = confP.getText().toString();

        if (TextUtils.isEmpty(password)) {
            newP.setError("Required.");
            valid = false;
        } else if(TextUtils.isEmpty(password2)){
            confP.setError("Required.");
        }else if(!password.equals(password2)){
            newP.setError("Password not the same");
            confP.setError("Password not the same");
            valid = false;
        }else {
            newP.setError(null);
            confP.setError(null);
        }

        return valid;
    }

}

