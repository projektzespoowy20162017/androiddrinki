package net.lubiejewski.drinkicrud.Controller.Drink;

import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.APIRequest;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

public class DrinkFilter extends Fragment {

    private ProgressDialog dialog;
    private String APIurl;

    ArrayList<HashMap<String, String>> skladnikMap = new ArrayList<>();
    private GridView grid;
    private int wybrany;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_drink_filter, container, false);

        APIurl = getString(R.string.serwer) + getString(R.string.table_skladnik) + "?sort=nazwa";

        getActivity().setTitle(getString(R.string.title_activity_drink_lista));

        setHasOptionsMenu(true);

        getIngredients();

        grid = (GridView) view.findViewById(R.id.filter_grid);
        grid.setNumColumns(3);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //noinspection unchecked
                HashMap<String, String> skladnik = (HashMap<String, String>) adapterView.getItemAtPosition((int) l);
                wybrany = Integer.parseInt(skladnik.get("id"));

                Bundle filtr = new Bundle();
                filtr.putInt("idSkladnik", wybrany);
                DrinkListaFragment listaFragment = new DrinkListaFragment();
                listaFragment.setArguments(filtr);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_fragment, listaFragment).addToBackStack("lista").commit();
            }
        });

        return view;
    }

    public void getIngredients() {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.progress_dialog));
        dialog.show();

        skladnikMap = new ArrayList<>();
        HashMap<String, String> skladnikHash = new HashMap<>();
        skladnikHash.put("id", "0");
        skladnikHash.put("nazwa", getString(R.string.filter_all));
        skladnikMap.add(skladnikHash);
        APIRequest request = new APIRequest(Request.Method.GET, APIurl, null, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray skladniki = jsonObject.getJSONArray("ingredients");
                    for (int i = 0; i < skladniki.length(); i++) {
                        JSONObject skladnikJSON = skladniki.getJSONObject(i);

                        if (skladnikJSON.getBoolean("alkohol")) {
                            HashMap<String, String> skladnikHash = new HashMap<>();
                            skladnikHash.put("id", skladnikJSON.getString("id"));
                            skladnikHash.put("nazwa",skladnikJSON.getString("nazwa"));
                            skladnikMap.add(skladnikHash);
                        }
                    }

                    ListAdapter adapter = new SimpleAdapter(
                            getActivity(), skladnikMap,
                            R.layout.filter_item, new String[] { "nazwa"},
                            new int[] { R.id.filter_name });
                    grid.setAdapter(adapter);

                    if (dialog.isShowing()) dialog.dismiss();
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), getString(R.string.data_download_error), Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance(getActivity()).addToRequestQueue(request);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Bundle filtr = new Bundle();
                filtr.putString("nameSearch", "nazwa=" + s);
                DrinkListaFragment listaFragment = new DrinkListaFragment();
                listaFragment.setArguments(filtr);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_fragment, listaFragment).addToBackStack("lista").commit();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        menu.setGroupVisible(R.id.menu_sort, false);

        super.onPrepareOptionsMenu(menu);
    }
}
