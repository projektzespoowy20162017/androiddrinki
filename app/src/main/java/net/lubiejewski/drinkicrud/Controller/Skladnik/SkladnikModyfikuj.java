package net.lubiejewski.drinkicrud.Controller.Skladnik;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.MainActivity;
import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class SkladnikModyfikuj extends AppCompatActivity{
    private String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog dialog;
    String APIurl;
    String authToken;

    JSONObject obiekt;
    Skladnik dane;

    int id;
    EditText nazwaText;
    EditText producentText;
    EditText jednostkaText;
    Switch alkoholSwitch;

    private boolean propositionsFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skladnik_formularz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        propositionsFlag = getIntent().getBooleanExtra("mode", false);

        id = getIntent().getIntExtra("id_skladnik",0);
        nazwaText = (EditText) findViewById(R.id.add_nazwa_skladnik);
        producentText = (EditText) findViewById(R.id.add_producent);
        jednostkaText = (EditText) findViewById(R.id.add_jednostka);
        alkoholSwitch = (Switch) findViewById(R.id.alkohol_switch);
        APIurl = getString(R.string.serwer) + getString(R.string.table_skladnik) + "/" + id;

        dane = new Skladnik();

        dane.setId_skladnik(id);
        dane.setNazwa(getIntent().getStringExtra("nazwa"));
        dane.setProducent(getIntent().getStringExtra("producent"));
        setValues();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modyfikujSkladnik();
            }
        });
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setValues(){
        new getOne().execute();
    }

    private void modyfikujSkladnik(){
        obiekt = new JSONObject();

        try {
            obiekt.put("nazwa", nazwaText.getText().toString());
            obiekt.put("producent", producentText.getText().toString());
            obiekt.put("jednostka",jednostkaText.getText().toString());
            if(alkoholSwitch.isChecked()) obiekt.put("alkohol",true);
            else obiekt.put("alkohol",false);
            obiekt.put("status", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new modyfikuj().execute();

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private class getOne extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(SkladnikModyfikuj.this);
            dialog.setMessage(getString(R.string.progress_dialog));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            HttpHandler sh = new HttpHandler();
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) {
                authToken = "Bearer " + user.getToken(false).getResult().getToken();
                String jsonStr = sh.makeServiceCall(APIurl, "GET", authToken);

                if (jsonStr != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(jsonStr);
                        JSONObject skladnikJSON;
                        skladnikJSON = jsonObj.getJSONObject("ingredient");
                        int id = skladnikJSON.getInt("id");
                        String nazwa = skladnikJSON.getString("nazwa");
                        String producent = skladnikJSON.getString("producent");

                        dane.setId_skladnik(id);
                        dane.setNazwa(nazwa);
                        dane.setProducent(producent);
                        dane.setJednostka(skladnikJSON.getString("jednostka"));
                        dane.setAlkohol(skladnikJSON.getBoolean("alkohol"));
                    } catch (final JSONException e) {
                        Log.e(TAG, "Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        getString(R.string.data_download_error_2),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });

                    }
                } else {
                    Log.e(TAG, "Couldn't get json from server.");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    nazwaText.setText(dane.getNazwa(), TextView.BufferType.EDITABLE);
                    producentText.setText(dane.getProducent(), TextView.BufferType.EDITABLE);
                    jednostkaText.setText(dane.getJednostka(), TextView.BufferType.EDITABLE);
                    alkoholSwitch.setChecked(dane.isAlkohol());
                }});
            if (dialog.isShowing()) dialog.dismiss();
        }
    }

    private class modyfikuj extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                URL url = new URL(APIurl);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("PUT");
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Content-Type","application/json");
                conn.setRequestProperty("Authorization",authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(obiekt.toString());
                writer.flush();
                writer.close();



                if(conn.getResponseCode() == 201){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.successful_modify),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
