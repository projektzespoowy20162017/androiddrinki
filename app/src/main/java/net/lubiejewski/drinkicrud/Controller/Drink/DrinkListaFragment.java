package net.lubiejewski.drinkicrud.Controller.Drink;


import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.DrinkAdapter;
import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.MainActivity;
import net.lubiejewski.drinkicrud.Model.Drink;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class DrinkListaFragment extends Fragment {
    private String TAG = MainActivity.class.getSimpleName();

    FirebaseAuth.AuthStateListener authListener;
    private ArrayAdapter<Drink> adapter;

    private ProgressDialog dialog;
    private ListView list;

    private String baseOperationsAPIurl;
    private String baseAPIurl;
    private String APIurl;
    private String deleteImageURL;
    private String authToken;

    private int wybrany;
    private String wybranyTytul;
    private String wybranyPrzepis;
    private float wybranyOcena;
    private int wybranyObrazek;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_drink_lista, container, false);

        setHasOptionsMenu(true);

        String filter = null;
        Bundle args = this.getArguments();
        if(args != null){
            if(args.containsKey("idSkladnik")) {
                int wybrany = args.getInt("idSkladnik", 0);
                if (wybrany != 0) filter = "ingredient=" + wybrany;
            } else {
                if(args.containsKey("nameSearch")){
                    filter = args.getString("nameSearch");
                }
            }
        }

        deleteImageURL = getString(R.string.serwer) + getString(R.string.table_obrazek);
        baseOperationsAPIurl = getString(R.string.serwer) + getString(R.string.table_drink);
        baseAPIurl = baseOperationsAPIurl + "?";
        if(filter != null) baseAPIurl += filter;
        APIurl = baseAPIurl;

        getActivity().setTitle(getString(R.string.title_activity_drink_lista));

        populateListView();

        list = (ListView) view.findViewById(R.id.lista_drink);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Drink drink = (Drink) adapterView.getItemAtPosition((int) l);
                wybrany = drink.getId();
                wybranyTytul = drink.getNazwa();
                wybranyPrzepis = drink.getPrzepis();
                wybranyOcena = drink.getOcena();
                wybranyObrazek = drink.getObrazekID();
                getActivitySzczegoly();
            }
        });

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
                ListView list = (ListView) getActivity().findViewById(R.id.lista_drink);
                if (user != null) {
                    fab.setVisibility(View.VISIBLE);
                    list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
                        @Override
                        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l){
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if(user!=null) {
                                String token = user.getToken(false).getResult().getToken();
                                authToken = "Bearer" + token;
                                Drink drink = (Drink) adapterView.getItemAtPosition((int) l);
                                String currentUserID = user.getUid();
                                String authorID = drink.getAutor();
                                if (currentUserID.equals(authorID) || AppController.getInstance(getActivity()).isAdmin()) {
                                    wybrany = drink.getId();
                                    wybranyTytul = drink.getNazwa();
                                    wybranyPrzepis = drink.getPrzepis();
                                    wybranyOcena = drink.getOcena();
                                    wybranyObrazek = drink.getObrazekID();
                                    onCreatePopupMenu(view);
                                }
                            }
                            return true;
                        }
                    });
                } else {
                    fab.setVisibility(View.GONE);
                    list.setOnItemLongClickListener(null);
                }
            }
        };
        FirebaseAuth.getInstance().addAuthStateListener(authListener);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivityDodaj();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                populateListView();
            }
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

        menu.setGroupVisible(R.id.menu_sort, true);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.sort_by_name:
                String sortName = "&sort=nazwa";
                APIurl = baseAPIurl + sortName;
                populateListView();
                break;
            case R.id.sort_by_date:
                String sortDate = "&sort=created";
                APIurl = baseAPIurl + sortDate;
                populateListView();
                break;
            case R.id.sort_by_rating:
                String sortRating = "&sort=average";
                APIurl = baseAPIurl + sortRating;
                populateListView();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDetach(){
        FirebaseAuth.getInstance().removeAuthStateListener(authListener);
        super.onDetach();
    }

    public boolean onCreatePopupMenu(View view){
        PopupMenu popup = new PopupMenu(getActivity(),view);
        popup.getMenuInflater().inflate(R.menu.menu_list, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                onPopupMenuItemClick(item);

                return getActivity().onMenuItemSelected(id, item);
            }
        });
        popup.show();
        return true;
    }

    private void onPopupMenuItemClick(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.itemModify) {
            getActivityModyfikuj();
        }

        if (id == R.id.itemDelete){
            new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.delete_dialog_title))
                    .setMessage(getString(R.string.delete_dialog_content))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new deleteDrink().execute();
                            populateListView();
                        }
                    })
                    .setNegativeButton(R.string.no, null).show();
        }
    }

    private void getActivityModyfikuj(){
        Intent modyfikuj = new Intent(getActivity(), DrinkModyfikuj.class);
        modyfikuj.putExtra("id_drink", wybrany);
        modyfikuj.putExtra("tytul", wybranyTytul);
        modyfikuj.putExtra("przepis", wybranyPrzepis);
        modyfikuj.putExtra("authToken", authToken);
        startActivityForResult(modyfikuj, 1);
    }

    private void getActivityDodaj(){
        Intent dodaj = new Intent(getActivity(), DrinkDodaj.class);
        dodaj.putExtra("authToken", authToken);
        startActivityForResult(dodaj, 1);
    }

    private void getActivitySzczegoly(){
        Intent wyswietl = new Intent(getActivity(), DrinkSzczegoly.class);
        wyswietl.putExtra("id_drink", wybrany);
        wyswietl.putExtra("tytul", wybranyTytul);
        wyswietl.putExtra("przepis", wybranyPrzepis);
        wyswietl.putExtra("ocena", wybranyOcena);
        wyswietl.putExtra("authToken", authToken);
        startActivity(wyswietl);
    }

    private void populateListView(){
        new getList().execute();
    }

    private class deleteDrink extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Log.i("Usuwanie", "id_usuwany : " + wybrany);

                String usunURL = baseOperationsAPIurl + "/" + wybrany;

                Log.i("Adres usuwanego", "adres : " + usunURL);

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    authToken = "Bearer " + user.getToken(false).getResult().getToken();

                    URL url = new URL(usunURL);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("DELETE");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Authorization", authToken);
                    int response = conn.getResponseCode();
                    if (response == 200) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(),
                                        getString(R.string.successful_delete),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                        deleteImage();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private void deleteImage(){
            try {
                String usunURL = deleteImageURL + "/" + wybranyObrazek;
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null) {
                    authToken = "Bearer " + user.getToken(false).getResult().getToken();

                    URL url = new URL(usunURL);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("DELETE");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Authorization", authToken);
                    conn.getResponseCode();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class getList extends AsyncTask<Void, Void, Void>{
        ArrayList<Drink> drinkiLista;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage(getString(R.string.progress_dialog));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler handler = new HttpHandler();
            String jsonStr;
            jsonStr = handler.getDataNoToken(APIurl);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONArray drinki = jsonObj.getJSONArray("drinks");

                    drinkiLista = new ArrayList<>();

                    for (int i = 0; i < drinki.length(); i++) {
                        JSONObject drinkJSON = drinki.getJSONObject(i);

                        Drink drink = new Drink();
                        drink.setId(drinkJSON.getInt("id"));
                        drink.setNazwa(drinkJSON.getString("nazwa"));
                        drink.setPrzepis(drinkJSON.getString("przepis"));
                        drink.setAutor(drinkJSON.getString("user_id"));

                        if(!drinkJSON.isNull("file_id")) {
                            drink.setObrazekID(drinkJSON.getInt("file_id"));
                            JSONObject file = drinkJSON.getJSONObject("file");
                            String thumbnailURL = file.getString("thumbPath");
                            if(URLUtil.isValidUrl(thumbnailURL)) {
                                drink.setObrazek(new URL(thumbnailURL));
                            }
                            else {
                                drink.setObrazek(null);
                            }
                        }

                        drink.setOcena((float) drinkJSON.getDouble("average"));
                        drinkiLista.add(drink);
                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(),
                                getString(R.string.data_download_error),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (dialog.isShowing()) dialog.dismiss();

            adapter = new DrinkAdapter(getContext(), R.layout.item_drink, drinkiLista);

            list.setAdapter(adapter);
        }
    }
}
