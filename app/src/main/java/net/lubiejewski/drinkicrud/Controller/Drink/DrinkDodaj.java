package net.lubiejewski.drinkicrud.Controller.Drink;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.APIRequest;
import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.Controller.Helpers.MultipartRequest;
import net.lubiejewski.drinkicrud.Controller.Skladnik.SkladnikChoose;
import net.lubiejewski.drinkicrud.MainActivity;
import net.lubiejewski.drinkicrud.Model.Drink;
import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class DrinkDodaj extends AppCompatActivity {

    private String APIurl;
    private String DrinkSkladnikURL;
    private String authToken;

    JSONObject obiekt;
    JSONObject skladnikiJSON[];
    Drink dane;
    int iloscSkladnikow;
    private String thumbnailURL;

    EditText ilosci[];

    private HashMap<Integer, Skladnik> wybraneSkladnikiHashMap;
    private ArrayList<Skladnik> wybraneSkladniki;

    private String TAG = MainActivity.class.getSimpleName();

    private File photoFile;
    private Uri photoUri;

    private String imageURLString;
    private int imageID = 0;
    private boolean imageUploadProgress = true;
    private boolean deleteButtonClicked = false;
    private boolean photoExists = false;

    private final Context context = this;
    private final String twoHyphens = "--";
    private final String lineEnd = "\r\n";
    private final String boundary = "apiclient-" + System.currentTimeMillis();
    private final String mimeType = "multipart/form-data;boundary=" + boundary;
    private byte[] multipartBody;

    @SuppressLint({"UseSparseArrays", "SimpleDateFormat"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_formularz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        APIurl = getResources().getString(R.string.serwer) + getResources().getString(R.string.table_drink);
        DrinkSkladnikURL = getResources().getString(R.string.serwer) + getResources().getString(R.string.table_drink_has_skladnik);
        dane = new Drink();
        imageURLString = null;
        thumbnailURL = null;
        imageID = 0;

        try {
            File outputDir = this.getCacheDir();
            photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        wybraneSkladniki = new ArrayList<>();
        wybraneSkladnikiHashMap = new HashMap<>();

        // ---------------------------------------------------

        Button galleryButton = (Button) findViewById(R.id.add_image_intent);
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(DrinkDodaj.this, new String[] {android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                } else{
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    if(photoPickerIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(photoPickerIntent, 1);
                    }
                }
            }
        });

        galleryButton.setVisibility(View.VISIBLE);

        Button cameraButton = (Button) findViewById(R.id.capture_image_button);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View view) {
                if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(DrinkDodaj.this, new String[] {android.Manifest.permission.CAMERA}, 2);
                } else{
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(DrinkDodaj.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                    } else {
                        if (photoCapture.resolveActivity(getPackageManager()) != null) {
                            try {
                                if(photoFile!=null) {
                                    //noinspection ResultOfMethodCallIgnored
                                    photoFile.delete();
                                    File outputDir = Environment.getExternalStorageDirectory();
                                    photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                                }
                                if (photoFile != null) {
                                    photoUri = Uri.fromFile(photoFile);
                                    photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                                    startActivityForResult(photoCapture, 2);
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) cameraButton.setVisibility(View.GONE);

        // ---------------------------------------------------

        Button wybierzSkladniki = (Button) findViewById(R.id.chooseButton);
        wybierzSkladniki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppController.getInstance(view.getContext()).setWybraneSkladniki(wybraneSkladniki);
                Intent choose = new Intent(view.getContext(), SkladnikChoose.class);
                startActivityForResult(choose, 3);
            }
        });

        Button usunObrazek = (Button) findViewById(R.id.button_delete_image);
        usunObrazek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageID!=0) {
                    deleteButtonClicked = true;
                    deletePhoto();
                } else {
                    LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                    layout.setVisibility(View.GONE);
                    photoExists = false;
                }
            }
        });

        Button rotateLeft = (Button) findViewById(R.id.button_rotate_left);
        rotateLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotatePhoto(-90);
            }
        });

        Button rotateRight = (Button) findViewById(R.id.button_rotate_right);
        rotateRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rotatePhoto(90);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dodajDrinka();
            }
        });
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void dodajDrinka(){
        if(photoExists){
            uploadPhoto();
        }

        obiekt = new JSONObject();
        skladnikiJSON = new JSONObject[iloscSkladnikow];

        EditText nazwa = (EditText) findViewById(R.id.add_nazwa_drink);
        EditText przepis = (EditText) findViewById(R.id.add_przepis);

        try {
            obiekt.put("nazwa", nazwa.getText().toString());
            obiekt.put("przepis", przepis.getText().toString());
            if(imageID!=0) obiekt.put("file_id", imageID);
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) obiekt.put("user_id", user.getUid());

            int i = 0;
            for(Skladnik skladnik : wybraneSkladniki){
                skladnikiJSON[i] = new JSONObject();
                Skladnik pobrany = wybraneSkladnikiHashMap.get(skladnik.getId_skladnik());
                skladnikiJSON[i].put("ingredient_id", pobrany.getId_skladnik());
                skladnikiJSON[i].put("ilosc", pobrany.getIlosc());
                i++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new dodaj().execute();

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void buildPart(DataOutputStream dataOutputStream, byte[] fileData, String fileName) throws IOException {
        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"image\"; filename=\""
                + fileName + "\"" + lineEnd);
        dataOutputStream.writeBytes(lineEnd);

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        dataOutputStream.writeBytes(lineEnd);
    }

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case 1:
                //OBRAZEK
                if(resultCode == RESULT_OK && imageReturnedIntent != null){
                    photoExists = true;
                    Uri imageUri = imageReturnedIntent.getData();

                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(imageUri,filePathColumn, null, null, null);
                    if(cursor!=null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();

                        new copyImage(picturePath).execute();

                        LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                        layout.setVisibility(View.VISIBLE);
                        ImageView thumbnailView = (ImageView) findViewById(R.id.uploadThumbnail);
                        thumbnailView.setImageURI(Uri.parse(picturePath));
                    }
                }
                break;
            case 2:
                //APARAT
                if(photoUri != null) {
                    Bitmap src = decodeFile(photoFile, 960, 540);
                    photoExists = true;

                    LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                    layout.setVisibility(View.VISIBLE);
                    ImageView thumbnailView = (ImageView) findViewById(R.id.uploadThumbnail);
                    thumbnailView.setImageBitmap(src);
                }
                break;
            case 3:
                //SKLADNIKI
                wybraneSkladniki = AppController.getInstance(this).getWybraneSkladniki();
                wybraneSkladnikiHashMap = new HashMap<>();
                for(Skladnik skladnik : wybraneSkladniki){
                    wybraneSkladnikiHashMap.put(skladnik.getId_skladnik(), skladnik);
                    Log.d(TAG, "Dodano " + skladnik.getNazwa());
                }
                wyswietlSkladniki();
                break;
        }
    }

    private void wyswietlSkladniki(){
        if(wybraneSkladniki!=null){
            LinearLayout dodajSkladniki = (LinearLayout) findViewById(R.id.skladniki_drinka_formularz);
            dodajSkladniki.removeAllViews();
            iloscSkladnikow = wybraneSkladniki.size();
            Log.d(TAG, "Ilosc skladnikow: " + iloscSkladnikow);
            ilosci = new EditText[iloscSkladnikow];

            for(int i=0;i<iloscSkladnikow;i++){
                View linia = getLayoutInflater().inflate(R.layout.drink_formularz_skladnik_item, dodajSkladniki, false);
                Skladnik liniaSkladnik = wybraneSkladniki.get(i);

                TextView nazwa = (TextView) linia.findViewById(R.id.ingredient_name);
                nazwa.setText(liniaSkladnik.getNazwa());
                ilosci[i] = (EditText) linia.findViewById(R.id.ingredient_quantity);
                ilosci[i].setText(liniaSkladnik.getIlosc());
                onTextChange(ilosci[i], liniaSkladnik.getId_skladnik());
                TextView jednostka = (TextView) linia.findViewById(R.id.ingredient_unit);
                jednostka.setText(liniaSkladnik.getJednostka());
                Button przycisk = (Button) linia.findViewById(R.id.ingredient_delete);
                onButtonClick(przycisk, liniaSkladnik.getId_skladnik());

                dodajSkladniki.addView(linia);
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                } else{
                    Toast.makeText(context, getString(R.string.file_read_permissions_error), Toast.LENGTH_SHORT).show();
                }
                return;
            case 2:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(DrinkDodaj.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                    } else {
                        if (photoCapture.resolveActivity(getPackageManager()) != null) {
                            try {
                                File outputDir = this.getCacheDir();
                                photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                                photoUri = Uri.fromFile(photoFile);
                                photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                                startActivityForResult(photoCapture, 2);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Toast.makeText(context, getString(R.string.camera_permissions_error), Toast.LENGTH_SHORT).show();
                }
                return;
            case 3:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (photoCapture.resolveActivity(getPackageManager()) != null) {
                        try {
                            File outputDir = this.getCacheDir();
                            photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                            photoUri = Uri.fromFile(photoFile);
                            photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                            startActivityForResult(photoCapture, 2);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(context, getString(R.string.file_save_permissions_error), Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void rotatePhoto(int degree){
        Bitmap src = decodeFile(photoFile, 960, 540);
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        if (src != null) {
            try {
                src = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
                FileOutputStream fOut = new FileOutputStream(photoFile);
                src.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                ImageView thumbnailView = (ImageView) findViewById(R.id.uploadThumbnail);
                thumbnailView.setImageBitmap(src);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void deletePhoto(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null) {
            authToken = user.getToken(false).getResult().getToken();
            APIRequest request = new APIRequest(Request.Method.DELETE, getString(R.string.image_server) + "/" + imageID, authToken, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    Log.d(TAG, "Volley deletion Status code: " + response.statusCode);
                    if (deleteButtonClicked) {
                        Toast.makeText(getApplicationContext(), getString(R.string.successful_delete), Toast.LENGTH_SHORT).show();
                    }
                    LinearLayout layout = (LinearLayout) findViewById(R.id.drink_form_image);
                    layout.setVisibility(View.GONE);
                    imageID = 0;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (deleteButtonClicked) {
                        Toast.makeText(getApplicationContext(), getString(R.string.data_download_error) + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            AppController.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
    }

    private void uploadPhoto(){
        imageUploadProgress = false;
        Bitmap src = decodeFile(photoFile, 960, 540);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(src!=null) {
            src.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] imageData = baos.toByteArray();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            try {
                buildPart(dos, imageData, "image.jpeg");
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                multipartBody = bos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                authToken = "Bearer " + user.getToken(false).getResult().getToken();

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", authToken);

                MultipartRequest multipartRequest = new MultipartRequest(getString(R.string.image_server), headers, mimeType, multipartBody, new Response.Listener<NetworkResponse>() {
                    @SuppressWarnings("ResultOfMethodCallIgnored")
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);
                            jsonObject = jsonObject.getJSONObject("response");
                            if (!jsonObject.getBoolean("success")) {
                                Log.d(TAG, jsonObject.getString("message"));
                                Toast.makeText(context, getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Rzeczywiście upload success");
                                imageURLString = jsonObject.getString("imgURL");
                                thumbnailURL = jsonObject.getString("thumbnail");
                                imageID = jsonObject.getInt("id");
                                photoFile.delete();
                                imageUploadProgress = true;
                            }
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();
                    }
                });

                AppController.getInstance(context).addToRequestQueue(multipartRequest);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(photoUri!=null) {
            outState.putString("photoUri", photoUri.toString());
        }
        if(imageURLString!=null){
            outState.putString("imageURLString", imageURLString);
        }
        if(thumbnailURL!=null){
            outState.putString("thumbnailURL", thumbnailURL);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("photoUri")) {
                photoUri = Uri.parse(savedInstanceState.getString("photoUri"));
            }
            if (savedInstanceState.containsKey("imageURLString")) {
                imageURLString = savedInstanceState.getString("imageURLString");
            }
            if (savedInstanceState.containsKey("thumbnailURL")) {
                thumbnailURL = savedInstanceState.getString("thumbnailURL");
            }
        }

        super.onRestoreInstanceState(savedInstanceState);
    }

    public static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            int scale=1;
            while(o.outWidth/scale/2>= WIDTH && o.outHeight/scale/2>= HIGHT)
                scale*=2;
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException ignored) {}
        return null;
    }

    private void onTextChange(EditText text, int skladnikID){
        final int idSkladnik = skladnikID;
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                wybraneSkladnikiHashMap.get(idSkladnik).setIlosc(editable.toString());
            }
        });
    }

    private void onButtonClick(Button button, int skladnikID){
        final int idSkladnik = skladnikID;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout linearParent =  (LinearLayout) view.getParent().getParent();
                RelativeLayout linearChild = (RelativeLayout) view.getParent();
                linearParent.removeView(linearChild);
                wybraneSkladnikiHashMap.remove(idSkladnik);
                int index = 0;
                for(Skladnik skladnik : wybraneSkladniki){
                    if(skladnik.getId_skladnik()==idSkladnik){
                        index = wybraneSkladniki.indexOf(skladnik);
                    }
                }
                wybraneSkladniki.remove(index);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(photoFile.exists()){
            //noinspection ResultOfMethodCallIgnored
            photoFile.delete();
        }
        wybraneSkladniki = new ArrayList<>();
        AppController.getInstance(this).setWybraneSkladniki(wybraneSkladniki);
    }

    private class dodaj extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                URL url = new URL(APIurl);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                int idDrink;

                //noinspection StatementWithEmptyBody
                while (!imageUploadProgress){
                }
                if(imageID!=0) obiekt.put("file_id", imageID);

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null) {
                    authToken = "Bearer " + user.getToken(false).getResult().getToken();

                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Authorization", authToken);

                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                    writer.write(obiekt.toString());
                    writer.flush();
                    writer.close();

                    idDrink = pobierzID(conn);

                    for (int i = 0; i < wybraneSkladnikiHashMap.size(); i++) {
                        dodajSkladnik(idDrink, i);
                    }
                    if (conn.getResponseCode() == 201) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        getString(R.string.successful_save),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                    }
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private int pobierzID(HttpsURLConnection conn) throws IOException {
            int id = 0;
            HttpHandler handler = new HttpHandler();
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String response = handler.convertStreamToString(in);
            if(response != null){
                try{
                    JSONObject jsonObj = new JSONObject(response);
                    JSONObject data = jsonObj.getJSONObject("data");
                    id = data.getInt("id");
                    Log.i(TAG, "ID dodanego drinka: " + id);
                }
                catch (final JSONException e){
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
            return id;
        }

        private void dodajSkladnik(int idDrink, int idObiekt){
            try {
                skladnikiJSON[idObiekt].put("drink_id",idDrink);
                URL url = new URL(DrinkSkladnikURL);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Content-Type","application/json");
                conn.setRequestProperty("Authorization",authToken);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(skladnikiJSON[idObiekt].toString());
                writer.flush();
                writer.close();
                Log.d(TAG,"HTTP Response code(skladnik): " + conn.getResponseCode() + ", ze strony: " + DrinkSkladnikURL);
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class copyImage extends AsyncTask<Void,Void,Void> {
        private String sourcePath;

        copyImage(String source){
            this.sourcePath = source;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                File source = new File(sourcePath);
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(photoFile).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
