package net.lubiejewski.drinkicrud.Controller.User;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import net.lubiejewski.drinkicrud.Controller.Drink.DrinkModyfikuj;
import net.lubiejewski.drinkicrud.Controller.Drink.DrinkSzczegoly;
import net.lubiejewski.drinkicrud.Controller.Helpers.DrinkAdapter;
import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.Model.Drink;
import net.lubiejewski.drinkicrud.Model.User;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

public class UserProfile extends AppCompatActivity {
    private String TAG = UserProfile.class.getSimpleName();
    private ProgressDialog dialog;
    String UserUrl;
    String DrinkUrl;
    private String deleteImageURL;

    ArrayList<HashMap<String, String>> drinkMap;
    HttpHandler sh;
    private String authToken;

    private User dane;
    private FirebaseUser user;

    private int wybrany;
    private String wybranyTytul;
    private String wybranyPrzepis;
    private float wybranyOcena;
    private int wybranyObrazek;

    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        user = FirebaseAuth.getInstance().getCurrentUser();

        dane = new User();
        deleteImageURL = getString(R.string.serwer) + getString(R.string.table_obrazek);
        UserUrl = getString(R.string.serwer) + getString(R.string.table_uzytkownik) + "/" + user.getUid();
        DrinkUrl = getString(R.string.serwer) + getString(R.string.table_drink);

        if(user.getDisplayName()!=null) setTitle(user.getDisplayName());
        else setTitle(user.getEmail());

        populateListView();

        list = (ListView) findViewById(R.id.user_profile_drinki);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Drink drink = (Drink) adapterView.getItemAtPosition((int) l);
                wybrany = drink.getId();
                wybranyTytul = drink.getNazwa();
                wybranyPrzepis = drink.getPrzepis();
                wybranyOcena = drink.getOcena();
                wybranyObrazek = drink.getObrazekID();
                getActivitySzczegoly();
            }
        });
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l){
                Drink drink = (Drink) adapterView.getItemAtPosition((int) l);
                String currentUserID = user.getUid();
                String authorID = drink.getAutor();
                if(currentUserID.equals(authorID)) {
                    wybrany = drink.getId();
                    wybranyTytul = drink.getNazwa();
                    wybranyPrzepis = drink.getPrzepis();
                    wybranyOcena = drink.getOcena();
                    wybranyObrazek = drink.getObrazekID();
                    onCreatePopupMenu(view);
                }
                return true;
            }
        });

        Button changeUsernameBtn = (Button) findViewById(R.id.change_name);
        changeUsernameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent update = new Intent(getApplicationContext(), UserEdit.class);
                update.putExtra("mode",0);
                startActivityForResult(update, 2);
            }
        });

        Button changeAvatarBtn = (Button) findViewById(R.id.change_avatar);
        changeAvatarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent update = new Intent(getApplicationContext(), UserEdit.class);
                update.putExtra("mode",2);
                startActivityForResult(update, 2);
            }
        });

        Button changePasswordBtn = (Button) findViewById(R.id.change_password);
        changePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserProfile.this,ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                populateListView();
            }
        } else if(requestCode == 2){
            //TODO : odświeżanie profilu
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) user.reload();
        }
    }

    public boolean onCreatePopupMenu(View view){
        PopupMenu popup = new PopupMenu(UserProfile.this,view);
        popup.getMenuInflater().inflate(R.menu.menu_list, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                onPopupMenuItemClick(item);

                return onMenuItemSelected(id, item);
            }
        });
        popup.show();
        return true;
    }

    private void onPopupMenuItemClick(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.itemModify) {
            getActivityModyfikuj();
        }

        if (id == R.id.itemDelete){
            new deleteDrink().execute();
            populateListView();
        }
    }

    private void getActivityModyfikuj(){
        Intent modyfikuj = new Intent(UserProfile.this, DrinkModyfikuj.class);
        modyfikuj.putExtra("id_drink", wybrany);
        modyfikuj.putExtra("tytul", wybranyTytul);
        modyfikuj.putExtra("przepis", wybranyPrzepis);
        modyfikuj.putExtra("authToken", authToken);
        startActivityForResult(modyfikuj, 1);
    }

    private void getActivitySzczegoly(){
        Intent wyswietl = new Intent(UserProfile.this, DrinkSzczegoly.class);
        wyswietl.putExtra("id_drink", wybrany);
        wyswietl.putExtra("tytul", wybranyTytul);
        wyswietl.putExtra("przepis", wybranyPrzepis);
        wyswietl.putExtra("ocena", wybranyOcena);
        wyswietl.putExtra("authToken", authToken);
        startActivity(wyswietl);
    }

    private void populateListView(){
        drinkMap = new ArrayList<>();
        new getOne().execute();
    }

    private class deleteDrink extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Log.i("Usuwanie", "id_usuwany : " + wybrany);

                String usunURL = DrinkUrl + "/" + wybrany;

                Log.i("Adres usuwanego", "adres : " + usunURL);

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null) {
                    authToken = "Bearer " + user.getToken(false).getResult().getToken();

                    URL url = new URL(usunURL);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("DELETE");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Authorization", authToken);
                    int response = conn.getResponseCode();
                    if (response == 200) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(UserProfile.this,
                                        getString(R.string.successful_delete),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                        deleteImage();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private void deleteImage(){
            try {
                String usunURL = deleteImageURL + "/" + wybranyObrazek;
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user!=null) {
                    authToken = "Bearer " + user.getToken(false).getResult().getToken();

                    URL url = new URL(usunURL);
                    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
                    conn.setRequestMethod("DELETE");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Authorization", authToken);
                    conn.getResponseCode();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class getOne extends AsyncTask<Void, Void, Void> {
        String imageURL;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            dialog = new ProgressDialog(UserProfile.this);
            dialog.setMessage(getString(R.string.progress_dialog));
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            sh = new HttpHandler();
            String jsonStr = sh.getDataNoToken(UserUrl);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONObject userJSON = jsonObj.getJSONObject("user");
                    JSONArray drinksJSON = userJSON.getJSONArray("drinks");

                    dane.setID(userJSON.getString("id"));
                    dane.setName(userJSON.getString("username"));
                    dane.setActive(userJSON.getBoolean("active"));
                    dane.setAdmin(userJSON.getBoolean("isAdmin"));

                    if(!userJSON.isNull("file_id")){
                        int file_id=userJSON.getInt("file_id");
                        JSONArray pliki = userJSON.getJSONArray("files");
                        for(int i=0; i<pliki.length(); i++){
                            JSONObject plik = pliki.getJSONObject(i);
                            if(plik.getInt("id")==file_id){
                                imageURL = plik.getString("path");
                            }
                        }
                    } else {
                        imageURL = "https://images.pexels.com/photos/160150/pexels-photo-160150.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb";
                    }

                    ArrayList<Drink> drinks = new ArrayList<>();

                    for (int i = 0; i < drinksJSON.length(); i++) {
                        JSONObject drinkJSON = drinksJSON.getJSONObject(i);

                        Drink drink = new Drink();
                        drink.setId(drinkJSON.getInt("id"));
                        drink.setNazwa(drinkJSON.getString("nazwa"));
                        drink.setPrzepis(drinkJSON.getString("przepis"));
                        drink.setAutor(drinkJSON.getString("user_id"));

                        if(!drinkJSON.isNull("file_id")) {
                            drink.setObrazekID(drinkJSON.getInt("file_id"));
                            JSONObject file = drinkJSON.getJSONObject("file");
                            String thumbnailURL = file.getString("thumbPath");
                            if(URLUtil.isValidUrl(thumbnailURL)) {
                                Log.d(TAG, "Jest obrazek: " + thumbnailURL);
                                drink.setObrazek(new URL(thumbnailURL));
                            }
                            else {
                                Log.d(TAG,"Nie ma obrazka");
                                drink.setObrazek(null);
                            }
                        }

                        drink.setOcena((float) drinkJSON.getDouble("average"));
                        drinks.add(drink);
                    }

                    dane.setDrinks(drinks);

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.data_download_error),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        private boolean setListViewHeightBasedOnItems(ListView listView) {

            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter != null) {

                int numberOfItems = listAdapter.getCount();

                // Get total height of all items.
                int totalItemsHeight = 0;
                for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                    View item = listAdapter.getView(itemPos, null, listView);
                    item.measure(0, 0);
                    totalItemsHeight += item.getMeasuredHeight();
                }

                // Get total height of all item dividers.
                int totalDividersHeight = listView.getDividerHeight() *
                        (numberOfItems - 1);

                // Set list height.
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = totalItemsHeight + totalDividersHeight;
                listView.setLayoutParams(params);
                listView.requestLayout();

                return true;

            } else {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ImageView imageView = (ImageView) findViewById(R.id.user_photo);
            Picasso.with(getApplicationContext()).load(imageURL).into(imageView);

            ArrayAdapter<Drink> adapter = new DrinkAdapter(getBaseContext(), R.layout.item_drink, dane.getDrinks());
            list.setAdapter(adapter);
            setListViewHeightBasedOnItems(list);

            if (dialog.isShowing()) dialog.dismiss();
        }
    }
}
