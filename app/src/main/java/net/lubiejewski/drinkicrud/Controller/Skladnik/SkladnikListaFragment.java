package net.lubiejewski.drinkicrud.Controller.Skladnik;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.APIRequest;
import net.lubiejewski.drinkicrud.Controller.Helpers.SkladnikAdapter;
import net.lubiejewski.drinkicrud.Model.Skladnik;
import net.lubiejewski.drinkicrud.Model.User;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class SkladnikListaFragment extends Fragment {
    private String TAG = SkladnikListaFragment.class.getSimpleName();

    FirebaseAuth.AuthStateListener authListener;

    private ProgressDialog dialog;
    private ListView list;
    private ArrayAdapter<Skladnik> adapter;

    private String APIurl;
    private String baseAPIUrl;
    private String authToken;

    private int wybrany;
    private String wybranyNazwa;
    private String wybranyProducent;

    private boolean propozycjeFlag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_skladnik_lista, container, false);

        propozycjeFlag = false;
        Bundle args = this.getArguments();
        if(args != null){
            if(args.containsKey("mode")) {
                propozycjeFlag = args.getBoolean("mode");
            }
        }
        if(propozycjeFlag){
            baseAPIUrl = getString(R.string.serwer) + getString(R.string.table_skladnik);
            APIurl = getString(R.string.propositions_link);
            getActivity().setTitle(getString(R.string.title_activity_propozycje_lista));
        } else {
            baseAPIUrl = getString(R.string.serwer) + getString(R.string.table_skladnik);
            APIurl = baseAPIUrl + "?sort=nazwa";
            getActivity().setTitle(getString(R.string.title_activity_skladnik_lista));
        }

        setHasOptionsMenu(true);

        populateListView();

        list = (ListView) view.findViewById(R.id.lista_skladnik);

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
                ListView list = (ListView) getActivity().findViewById(R.id.lista_skladnik);
                if (user != null) {
                    if(propozycjeFlag){
                        fab.setVisibility(View.GONE);
                    } else {
                        fab.setVisibility(View.VISIBLE);
                    }
                    list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
                        @Override
                        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l){
                            Skladnik skladnik = (Skladnik) adapterView.getItemAtPosition((int) l);
                            wybrany = skladnik.getId_skladnik();
                            wybranyNazwa = skladnik.getNazwa();
                            wybranyProducent = skladnik.getProducent();
                            onCreatePopupMenu(view);
                            return true;
                        }
                    });
                } else {
                    fab.setVisibility(View.GONE);
                    list.setOnItemLongClickListener(null);
                }
            }
        };
        FirebaseAuth.getInstance().addAuthStateListener(authListener);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivityDodaj();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                AppController.getInstance(getActivity()).getRequestQueue().getCache().clear();
                populateListView();
            }
        }
    }

    @Override
    public void onDetach(){
        FirebaseAuth.getInstance().removeAuthStateListener(authListener);
        super.onDetach();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

        super.onPrepareOptionsMenu(menu);
    }

    public boolean onCreatePopupMenu(View view){
        PopupMenu popup = new PopupMenu(getActivity(),view);
        popup.getMenuInflater().inflate(R.menu.menu_list, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                onPopupMenuItemClick(item);

                return getActivity().onMenuItemSelected(id, item);
            }
        });
        popup.show();
        return true;
    }

    private void onPopupMenuItemClick(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.itemModify) {
            getActivityModyfikuj();
        }
        if (id == R.id.itemDelete){
            deleteIngredientDialog();
        }
    }

    private void getActivityModyfikuj(){
        Intent modyfikuj = new Intent(getActivity(), SkladnikModyfikuj.class);
        if(propozycjeFlag){
            modyfikuj.putExtra("mode",true);
        } else {
            modyfikuj.putExtra("mode", false);
        }
        modyfikuj.putExtra("id_skladnik", wybrany);
        modyfikuj.putExtra("nazwa", wybranyNazwa);
        modyfikuj.putExtra("producent", wybranyProducent);
        modyfikuj.putExtra("authToken",authToken);
        startActivityForResult(modyfikuj, 1);
    }

    private void getActivityDodaj(){
        Intent dodaj = new Intent(getActivity(), SkladnikDodaj.class);
        dodaj.putExtra("authToken",authToken);
        dodaj.putExtra("mode",false);
        startActivityForResult(dodaj, 1);
    }

    private void deleteIngredientDialog(){
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.delete_dialog_title))
                .setMessage(getString(R.string.delete_dialog_content))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteIngredient();
                        //new deleteSkladnik().execute();
                        populateListView();
                        if(dialog.isShowing()) dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.no, null).show();
    }

    private void deleteIngredient(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null) {
            authToken = user.getToken(false).getResult().getToken();
            APIRequest request = new APIRequest(Request.Method.DELETE, baseAPIUrl + "/" + wybrany, authToken, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    Log.d(TAG, "Volley deletion Status code: " + response.statusCode);
                    try {
                        Log.d(TAG, "Volley deletion message: " + new String(response.data, HttpHeaderParser.parseCharset(response.headers)));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getActivity(), getString(R.string.successful_delete), Toast.LENGTH_SHORT).show();
                    populateListView();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), getString(R.string.data_download_error) + error.toString(), Toast.LENGTH_SHORT).show();
                    try {
                        Log.d(TAG, "Volley deletion message: " + new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers)));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            });
            AppController.getInstance(getActivity()).addToRequestQueue(request);
        }
    }

    private void populateListView(){
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.progress_dialog));
        dialog.show();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null) authToken = user.getToken(false).getResult().getToken();
        APIRequest request = new APIRequest(Request.Method.GET, APIurl, authToken, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    JSONObject jsonObject = new JSONObject(jsonString);
                    JSONArray skladniki;
                    skladniki = jsonObject.getJSONArray("ingredients");
                    ArrayList<Skladnik> skladnikiLista = new ArrayList<>();
                    for (int i = 0; i < skladniki.length(); i++) {
                        JSONObject skladnikJSON = skladniki.getJSONObject(i);
                        Skladnik skladnik = new Skladnik();
                        skladnik.setId_skladnik(skladnikJSON.getInt("id"));
                        skladnik.setNazwa(skladnikJSON.getString("nazwa"));
                        if(skladnikJSON.isNull("producent")) skladnik.setProducent("");
                        else skladnik.setProducent(skladnikJSON.getString("producent"));
                        skladnik.setAlkohol(skladnikJSON.getBoolean("alkohol"));
                        if (propozycjeFlag) {
                            JSONObject userJSON = skladnikJSON.getJSONObject("user");
                            User autor = new User();
                            autor.setID(userJSON.getString("id"));
                            if (!userJSON.isNull("name")) {
                                autor.setName(userJSON.getString("name"));
                            } else {
                                autor.setName(userJSON.getString("username"));
                            }
                            skladnik.setAutor(autor);
                        }
                        skladnikiLista.add(skladnik);
                    }
                    if(dialog.isShowing()) dialog.dismiss();
                    adapter = new SkladnikAdapter(getActivity(), R.layout.item_skladnik, skladnikiLista);
                    list = (ListView) getActivity().findViewById(R.id.lista_skladnik);
                    list.setAdapter(adapter);
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), getString(R.string.data_download_error), Toast.LENGTH_SHORT).show();
                if(dialog.isShowing()) dialog.dismiss();
            }
        });
        AppController.getInstance(getActivity()).addToRequestQueue(request);
    }
}
