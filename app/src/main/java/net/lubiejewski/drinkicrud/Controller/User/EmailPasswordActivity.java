package net.lubiejewski.drinkicrud.Controller.User;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.auth.UserProfileChangeRequest;

import net.lubiejewski.drinkicrud.MainActivity;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class EmailPasswordActivity extends BaseActivity implements View.OnClickListener{
    private static final String TAG = "EmailPassword";

    private EditText mEmailField;
    private EditText mPasswordField;
    private EditText mPasswordField2;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private JSONObject userData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emailpassword);

        mEmailField = (EditText) findViewById(R.id.field_email);
        mEmailField.setText(getIntent().getStringExtra("email"));
        mPasswordField = (EditText) findViewById(R.id.field_password);
        mPasswordField2 = (EditText) findViewById(R.id.field_password2);
        // Buttons
        findViewById(R.id.email_sign_in_button).setOnClickListener(this);
        findViewById(R.id.email_create_account_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void createAccount(final String email, final String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!createAccountValidateForm()) {
            return;
        }

        //czy już istnieje taki użytkownik
        mAuth.fetchProvidersForEmail(email).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                //noinspection ConstantConditions
                if(task.isSuccessful() && task.getResult().getProviders().size() == 1){
                    Toast.makeText(EmailPasswordActivity.this, getString(R.string.email_exists), Toast.LENGTH_SHORT).show();
                }
            }
        });


        showProgressDialog();

        // tworzymy użytkownika
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        if(task.isSuccessful()){
                            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                            if(user!=null) {
                                String name = email.split("@")[0];
                                try {
                                    userData = new JSONObject();
                                    userData.put("name", name);
                                    userData.put("username", email);
                                    userData.put("password", password);
                                    userData.put("id", user.getUid());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                new APIregister().execute();

                                UserProfileChangeRequest changeRequest = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(name).build();

                                user.updateProfile(changeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            user.reload();
                                        }
                                    }
                                });
                            }
                        }
                        if (!task.isSuccessful()) {
                            Toast.makeText(EmailPasswordActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();

                    }
                });

    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // logowanie użytkownika
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        if(task.isSuccessful()){
                            Intent intent = new Intent(EmailPasswordActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    void signOut() {
        mAuth.signOut();
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();

        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    private boolean createAccountValidateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        String password2 = mPasswordField2.getText().toString();

        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else if(TextUtils.isEmpty(password2)){
            mPasswordField2.setError("Required.");
        }else if(!password.equals(password2)){
            mPasswordField.setError("Password not the same");
            mPasswordField2.setError("Password not the same");
            valid = false;
        }else {
            mPasswordField.setError(null);
            mPasswordField2.setError(null);
        }

        return valid;
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.email_create_account_button) {
            createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
        } else if (i == R.id.email_sign_in_button) {
            signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
        } else if (i == R.id.sign_out_button) {
            signOut();
        } else if (i == R.id.add_confirm_pass){
            signUpUi();
        } else if (i == R.id.set_login_view){
            signInUi();
        }  else  if (i == R.id.forgot_pass){
            Intent intent = new Intent(EmailPasswordActivity.this, ResetPasswordActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void signUpUi(){
        findViewById(R.id.field_password2).setVisibility(View.VISIBLE);
        findViewById(R.id.email_sign_in_button).setVisibility(View.GONE);
        findViewById(R.id.email_create_account_button).setVisibility(View.VISIBLE);
        findViewById(R.id.add_confirm_pass).setVisibility(View.GONE);
        findViewById(R.id.set_login_view).setVisibility(View.VISIBLE);
        findViewById(R.id.valid_pass).setVisibility(View.VISIBLE);
        findViewById(R.id.forgot_pass).setVisibility(View.GONE);
    }
    public void signInUi(){
        findViewById(R.id.field_password2).setVisibility(View.GONE);
        findViewById(R.id.email_sign_in_button).setVisibility(View.VISIBLE);
        findViewById(R.id.add_confirm_pass).setVisibility(View.VISIBLE);
        findViewById(R.id.email_create_account_button).setVisibility(View.GONE);
        findViewById(R.id.set_login_view).setVisibility(View.GONE);
        findViewById(R.id.valid_pass).setVisibility(View.GONE);
        findViewById(R.id.forgot_pass).setVisibility(View.VISIBLE);
    }

    private class APIregister extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            user = FirebaseAuth.getInstance().getCurrentUser();
            register();
            return null;
        }

        private void register(){
            try{
                URL url = new URL(getString(R.string.serwer_register));
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Content-Type","application/json");

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(userData.toString());
                writer.flush();
                writer.close();

                if(conn.getResponseCode() == 201){
                    user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Email sent");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(EmailPasswordActivity.this, getString(R.string.email_confirmation),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }
                    });
                    Intent intent = new Intent(EmailPasswordActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Log.d(TAG, "API registration failed: " + conn.getResponseCode() + ", " + conn.getResponseMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(EmailPasswordActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                            user.delete();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
