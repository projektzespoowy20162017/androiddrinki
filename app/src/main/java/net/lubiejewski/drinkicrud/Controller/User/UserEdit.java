package net.lubiejewski.drinkicrud.Controller.User;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.squareup.picasso.Picasso;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Helpers.APIRequest;
import net.lubiejewski.drinkicrud.Controller.Helpers.HttpHandler;
import net.lubiejewski.drinkicrud.Controller.Helpers.MultipartRequest;
import net.lubiejewski.drinkicrud.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class UserEdit extends AppCompatActivity {
    private String TAG = UserEdit.class.getSimpleName();
    private int mode;
    FirebaseUser user;
    String authToken;

    JSONObject userData;
    String name;
    int imageID;

    private File photoFile;
    private Uri photoUri;
    private String imageURLString;
    private String thumbnailURL;

    private String userAPI;

    final static int CHANGE_USERNAME = 0;
    final static int CHANGE_PASSWORD = 1;
    final static int CHANGE_AVATAR = 2;

    private boolean photoModified = false;
    private boolean imageUploadProgress = true;
    private boolean deleteButtonClicked = false;
    private boolean photoExists = false;
    private boolean doNotDeleteFile = false;

    private final Context context = this;
    private final String twoHyphens = "--";
    private final String lineEnd = "\r\n";
    private final String boundary = "apiclient-" + System.currentTimeMillis();
    private final String mimeType = "multipart/form-data;boundary=" + boundary;
    private byte[] multipartBody;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mode = getIntent().getIntExtra("mode", 0);
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null) {
            userAPI = getString(R.string.serwer) + getString(R.string.table_uzytkownik) + "/" + user.getUid();
        }
        LinearLayout layout;

        if(mode == CHANGE_USERNAME){
            layout = (LinearLayout) findViewById(R.id.user_form_username);
        } else if(mode == CHANGE_PASSWORD){
            layout = (LinearLayout) findViewById(R.id.user_form_password);
        } else {
            layout = (LinearLayout) findViewById(R.id.user_form_image);

            new getLargeImage().execute();
            imageID = 0;
            try {
                File outputDir = this.getCacheDir();
                photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(user.getPhotoUrl()!=null){
                new downloadImage().execute();
            }
            Button galleryButton = (Button) findViewById(R.id.user_form_gallery);
            galleryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(UserEdit.this, new String[] {android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    } else{
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        if(photoPickerIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(photoPickerIntent, 1);
                        }
                    }
                }
            });

            galleryButton.setVisibility(View.VISIBLE);

            Button cameraButton = (Button) findViewById(R.id.user_form_camera);
            cameraButton.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SimpleDateFormat")
                @Override
                public void onClick(View view) {
                    if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(UserEdit.this, new String[] {android.Manifest.permission.CAMERA}, 2);
                    } else{
                        Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if(ContextCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(UserEdit.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                        } else {
                            if (photoCapture.resolveActivity(getPackageManager()) != null) {
                                try {
                                    if(photoFile!=null) {
                                        photoFile.delete();
                                        File outputDir = Environment.getExternalStorageDirectory();
                                        photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                                    }
                                    if (photoFile != null) {
                                        photoUri = Uri.fromFile(photoFile);
                                        photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                                        startActivityForResult(photoCapture, 2);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            });
            if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) cameraButton.setVisibility(View.GONE);

            Button usunObrazek = (Button) findViewById(R.id.button_delete_image);
            usunObrazek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(imageID!=0) {
                        deleteButtonClicked = true;
                        deletePhoto();
                    } else {
                        LinearLayout imageLayout = (LinearLayout) findViewById(R.id.user_form_display_image);
                        imageLayout.setVisibility(View.GONE);
                        photoExists = false;
                    }
                }
            });

            Button rotateLeft = (Button) findViewById(R.id.button_rotate_left);
            rotateLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rotatePhoto(-90);
                }
            });

            Button rotateRight = (Button) findViewById(R.id.button_rotate_right);
            rotateRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rotatePhoto(90);
                }
            });
        }
        layout.setVisibility(View.VISIBLE);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUserData();
            }
        });
        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void editUserData(){
        if(mode == CHANGE_USERNAME){
            try {
                EditText text = (EditText) findViewById(R.id.form_username);
                name = text.getText().toString();
                userData = new JSONObject();
                userData.put("name", name);

                new updateProfile().execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //avatar
            if(photoModified){
                try {
                    if (imageID != 0) deletePhoto();
                    if (photoFile != null && photoExists) uploadPhoto();
                    userData = new JSONObject();
                    if (imageID != 0) userData.put("file_id", imageID);
                    new updateProfile().execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            int scale=1;
            while(o.outWidth/scale/2>= WIDTH && o.outHeight/scale/2>= HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException ignored) {
        }
        return null;
    }

    private void rotatePhoto(int degree){
        Bitmap src = decodeFile(photoFile, 960, 540);
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        if (src != null) {
            try {
                photoModified = true;
                src = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
                FileOutputStream fOut = new FileOutputStream(photoFile);
                src.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                ImageView thumbnailView = (ImageView) findViewById(R.id.uploadAvatar);
                thumbnailView.setImageBitmap(src);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void deletePhoto(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null) {
            authToken = user.getToken(false).getResult().getToken();
            APIRequest request = new APIRequest(Request.Method.DELETE, getString(R.string.image_server) + "/" + imageID, authToken, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    Log.d(TAG, "Volley deletion Status code: " + response.statusCode);
                    if (deleteButtonClicked) {
                        Toast.makeText(getApplicationContext(), getString(R.string.successful_delete), Toast.LENGTH_SHORT).show();
                    }
                    LinearLayout layout = (LinearLayout) findViewById(R.id.user_form_display_image);
                    layout.setVisibility(View.GONE);
                    imageID = 0;
                    thumbnailURL = null;
                    imageURLString = null;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (deleteButtonClicked) {
                        Toast.makeText(getApplicationContext(), getString(R.string.data_download_error) + error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            AppController.getInstance(getApplicationContext()).addToRequestQueue(request);
        }
    }

    private void uploadPhoto(){
        imageUploadProgress = false;
        Bitmap src = decodeFile(photoFile, 960, 540);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(src!=null) {
            src.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] imageData = baos.toByteArray();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);
            try {
                buildPart(dos, imageData, "image.jpeg");
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                multipartBody = bos.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user!=null) {
                authToken = "Bearer " + user.getToken(false).getResult().getToken();

                Map<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", authToken);

                MultipartRequest multipartRequest = new MultipartRequest(getString(R.string.image_server), headers, mimeType, multipartBody, new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                            JSONObject jsonObject = new JSONObject(jsonString);
                            jsonObject = jsonObject.getJSONObject("response");
                            if (!jsonObject.getBoolean("success")) {
                                Log.d(TAG, jsonObject.getString("message"));
                                Toast.makeText(context, getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "Rzeczywiście upload success");
                                imageURLString = jsonObject.getString("imgURL");
                                thumbnailURL = jsonObject.getString("thumbnail");
                                imageID = jsonObject.getInt("id");
                                photoFile.delete();
                                imageUploadProgress = true;
                            }
                        } catch (UnsupportedEncodingException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();
                        imageUploadProgress = true;
                    }
                });

                AppController.getInstance(context).addToRequestQueue(multipartRequest);
            }
        }
    }

    private void buildPart(DataOutputStream dataOutputStream, byte[] fileData, String fileName) throws IOException {
        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"image\"; filename=\""
                + fileName + "\"" + lineEnd);
        dataOutputStream.writeBytes(lineEnd);

        ByteArrayInputStream fileInputStream = new ByteArrayInputStream(fileData);
        int bytesAvailable = fileInputStream.available();

        int maxBufferSize = 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dataOutputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        dataOutputStream.writeBytes(lineEnd);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case 1:
                //OBRAZEK
                if(resultCode == RESULT_OK && imageReturnedIntent != null){
                    photoModified = true;
                    photoExists = true;
                    Uri imageUri = imageReturnedIntent.getData();

                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = getContentResolver().query(imageUri,filePathColumn, null, null, null);
                    if(cursor!=null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();

                        new copyImage(picturePath).execute();

                        LinearLayout layout = (LinearLayout) findViewById(R.id.user_form_display_image);
                        layout.setVisibility(View.VISIBLE);
                        ImageView thumbnailView = (ImageView) findViewById(R.id.uploadAvatar);
                        thumbnailView.setImageURI(Uri.parse(picturePath));
                    }
                }
                break;
            case 2:
                //APARAT
                if(photoUri != null) {
                    Bitmap src = decodeFile(photoFile, 960, 540);
                    photoModified = true;
                    photoExists = true;

                    LinearLayout layout = (LinearLayout) findViewById(R.id.user_form_display_image);
                    layout.setVisibility(View.VISIBLE);
                    ImageView thumbnailView = (ImageView) findViewById(R.id.uploadAvatar);
                    thumbnailView.setImageBitmap(src);
                }
                break;
        }
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                } else{
                    Toast.makeText(UserEdit.this, getString(R.string.file_read_permissions_error), Toast.LENGTH_SHORT).show();
                }
                return;
            case 2:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if(ContextCompat.checkSelfPermission(UserEdit.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(UserEdit.this, new String[] {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
                    } else {
                        if (photoCapture.resolveActivity(getPackageManager()) != null) {
                            try {
                                File outputDir = this.getCacheDir();
                                photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                                photoUri = Uri.fromFile(photoFile);
                                photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

                                startActivityForResult(photoCapture, 2);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Toast.makeText(UserEdit.this, getString(R.string.camera_permissions_error), Toast.LENGTH_SHORT).show();
                }
                return;
            case 3:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Intent photoCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (photoCapture.resolveActivity(getPackageManager()) != null) {
                        try {
                            File outputDir = Environment.getExternalStorageDirectory();
                            photoFile = File.createTempFile("image" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()), ".jpg", outputDir);
                            photoUri = Uri.fromFile(photoFile);
                            photoCapture.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                            startActivityForResult(photoCapture, 2);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(UserEdit.this, getString(R.string.file_save_permissions_error), Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(photoUri!=null) {
            outState.putString("photoUri", photoUri.toString());
        }
        if(imageURLString!=null){
            outState.putString("imageURLString", imageURLString);
        }
        if(thumbnailURL!=null){
            outState.putString("thumbnailURL", thumbnailURL);
        }
        if(photoExists){
            outState.putInt("imageID", imageID);
            outState.putBoolean("photoExists", photoExists);
            outState.putBoolean("photoModified", photoModified);
            doNotDeleteFile = true;
            outState.putString("photoFile", photoFile.getAbsolutePath());
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("photoUri")) {
                photoUri = Uri.parse(savedInstanceState.getString("photoUri"));
            }
            if (savedInstanceState.containsKey("imageURLString")) {
                imageURLString = savedInstanceState.getString("imageURLString");
            }
            if (savedInstanceState.containsKey("thumbnailURL")) {
                thumbnailURL = savedInstanceState.getString("thumbnailURL");
            }
            if (savedInstanceState.containsKey("photoExists")){
                photoExists = true;
                imageID = savedInstanceState.getInt("imageID");
                photoModified = savedInstanceState.getBoolean("photoModified");
                //noinspection ConstantConditions
                photoFile = new File(savedInstanceState.getString("photoFile"));
                LinearLayout layout = (LinearLayout) findViewById(R.id.user_form_display_image);
                layout.setVisibility(View.VISIBLE);
                ImageView thumbnail = (ImageView) findViewById(R.id.uploadAvatar);
                Picasso.with(getApplicationContext()).load(photoFile).into(thumbnail);
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        if(photoFile!=null) {
            if (photoFile.exists()) {
                if (!doNotDeleteFile)
                    photoFile.delete();
            }
        }
        super.onDestroy();
    }

    private class updateProfile extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                URL url = new URL(userAPI);
                HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

                String token = "Bearer " + user.getToken(false).getResult().getToken();

                //noinspection StatementWithEmptyBody
                while (!imageUploadProgress){

                }
                if(imageID!=0) userData.put("file_id", imageID);

                conn.setRequestMethod("PUT");
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Content-Type","application/json");
                conn.setRequestProperty("Authorization",token);

                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                writer.write(userData.toString());
                writer.flush();
                writer.close();

                if(conn.getResponseCode() == 200){
                    if(mode == CHANGE_USERNAME){
                        UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder()
                                .setDisplayName(name).build();
                        user.updateProfile(profileChangeRequest);
                    } else if(mode == CHANGE_AVATAR) {
                        UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder()
                                .setPhotoUri(Uri.parse(thumbnailURL)).build();
                        user.updateProfile(profileChangeRequest);
                    }
                    user.reload().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(UserEdit.this, getString(R.string.profile_edit_success),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                    finish();
                } else {
                    Log.d(TAG, "User Profile edition failed: " + conn.getResponseCode() + ", " + conn.getResponseMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(UserEdit.this, getString(R.string.profile_edit_fail),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class getLargeImage extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            HttpHandler sh = new HttpHandler();
            String jsonStr = sh.getDataNoToken(userAPI);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONObject userJSON = jsonObj.getJSONObject("user");

                    if(!userJSON.isNull("file_id")){
                        int file_id=userJSON.getInt("file_id");
                        JSONArray pliki = userJSON.getJSONArray("files");
                        for(int i=0; i<pliki.length(); i++){
                            JSONObject plik = pliki.getJSONObject(i);
                            if(plik.getInt("id")==file_id){
                                imageURLString = plik.getString("path");
                            }
                        }
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.data_download_error_2),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.data_download_error),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }
            return null;
        }
    }

    private class downloadImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL imageURL = null;
                if(imageURLString!=null){
                    imageURL = new URL(imageURLString);
                } else if(user.getPhotoUrl()!=null) {
                    imageURL = new URL(user.getPhotoUrl().toString());
                }
                if(imageURL!=null) {
                    Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                    FileOutputStream fOutStream = new FileOutputStream(photoFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 50, fOutStream);
                    fOutStream.flush();
                    fOutStream.close();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            LinearLayout layout = (LinearLayout) findViewById(R.id.user_form_display_image);
                            layout.setVisibility(View.VISIBLE);
                            ImageView thumbnail = (ImageView) findViewById(R.id.uploadAvatar);
                            Picasso.with(getApplicationContext()).load(photoFile).into(thumbnail);
                            photoExists = true;
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class copyImage extends AsyncTask<Void,Void,Void> {
        private String sourcePath;

        copyImage(String source){
            this.sourcePath = source;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                File source = new File(sourcePath);
                if (source.exists()) {
                    FileChannel src = new FileInputStream(source).getChannel();
                    FileChannel dst = new FileOutputStream(photoFile).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
