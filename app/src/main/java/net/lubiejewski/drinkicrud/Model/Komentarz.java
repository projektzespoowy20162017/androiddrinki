package net.lubiejewski.drinkicrud.Model;

public class Komentarz {
    private int id;
    private User user;
    private String komentarz;
    private String dataDodania;
    private String dataModyfikacji;

    public Komentarz() {
    }

    public Komentarz(String komentarz, String dataDodania, String dataModyfikacji, int id) {
        this.komentarz = komentarz;
        this.dataDodania = dataDodania;
        this.dataModyfikacji = dataModyfikacji;
        this.id = id;
    }

    public Komentarz(int id, String komentarz, String dataDodania, String dataModyfikacji) {
        this.id = id;
        this.komentarz = komentarz;
        this.dataDodania = dataDodania;
        this.dataModyfikacji = dataModyfikacji;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getKomentarz() {
        return komentarz;
    }

    public void setKomentarz(String komentarz) {
        this.komentarz = komentarz;
    }

    public String getDataDodania() {
        return dataDodania;
    }

    public void setDataDodania(String dataDodania) {
        this.dataDodania = dataDodania;
    }

    public String getDataModyfikacji() {
        return dataModyfikacji;
    }

    public void setDataModyfikacji(String dataModyfikacji) {
        this.dataModyfikacji = dataModyfikacji;
    }
}
