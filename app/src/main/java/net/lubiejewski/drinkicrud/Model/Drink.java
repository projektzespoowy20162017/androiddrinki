package net.lubiejewski.drinkicrud.Model;

import com.google.gson.annotations.SerializedName;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Drink {
    private int id;
    @SerializedName("user_id")
    private String autor;
    private String nazwa;
    private String przepis;
    private float ocena;
    @SerializedName("imageURL")
    private URL obrazek;
    private int obrazekID;
    @SerializedName("ingredients")
    private List<Skladnik> skladniki;
    @SerializedName("comments")
    private List<Komentarz> komentarze;

    public Drink() {
        this.skladniki = new ArrayList<>();
        this.komentarze = new ArrayList<>();
    }

    public Drink(String nazwa, String przepis, float ocena) {
        this.nazwa = nazwa;
        this.przepis = przepis;
        this.ocena = ocena;
        this.skladniki = new ArrayList<>();
        this.komentarze = new ArrayList<>();
    }

    public Drink(int id, String nazwa, String przepis, float ocena) {
        this.id = id;
        this.nazwa = nazwa;
        this.przepis = przepis;
        this.ocena = ocena;
        this.skladniki = new ArrayList<>();
        this.komentarze = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getPrzepis() {
        return przepis;
    }

    public void setPrzepis(String przepis) {
        this.przepis = przepis;
    }

    public float getOcena() {
        return ocena;
    }

    public void setOcena(float ocena) {
        this.ocena = ocena;
    }

    public List<Skladnik> getSkladniki() {
        return skladniki;
    }

    public void setSkladniki(List<Skladnik> skladniki) {
        this.skladniki = skladniki;
    }

    public List<Komentarz> getKomentarze() {
        return komentarze;
    }

    public void setKomentarze(List<Komentarz> komentarze) {
        this.komentarze = komentarze;
    }

    public URL getObrazek() {
        return obrazek;
    }

    public void setObrazek(URL obrazek) {
        this.obrazek = obrazek;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getObrazekID() {
        return obrazekID;
    }

    public void setObrazekID(int obrazekID) {
        this.obrazekID = obrazekID;
    }
}
