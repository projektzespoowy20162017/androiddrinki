package net.lubiejewski.drinkicrud.Model;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class User {
    private String ID;
    private String name;
    private boolean isAdmin;
    private boolean active;
    private List<Drink> drinks;
    private URL obrazek;
    private int obrazekID;

    public User() {
        drinks = new ArrayList<>();
    }

    public User(String ID, boolean active, boolean isAdmin, String name) {
        this.ID = ID;
        this.active = active;
        this.isAdmin = isAdmin;
        this.name = name;
        drinks = new ArrayList<>();
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public URL getObrazek() {
        return obrazek;
    }

    public void setObrazek(URL obrazek) {
        this.obrazek = obrazek;
    }

    public int getObrazekID() {
        return obrazekID;
    }

    public void setObrazekID(int obrazekID) {
        this.obrazekID = obrazekID;
    }
}
