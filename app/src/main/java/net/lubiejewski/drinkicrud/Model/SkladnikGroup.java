package net.lubiejewski.drinkicrud.Model;

import java.util.ArrayList;

public class SkladnikGroup {

    private String name;
    private ArrayList<Skladnik> skladnikList = new ArrayList<>();

    public ArrayList<Skladnik> getSkladnikList() {
        return skladnikList;
    }

    public void setSkladnikList(ArrayList<Skladnik> skladnikList) {
        this.skladnikList = skladnikList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
