package net.lubiejewski.drinkicrud.Model;

public class Skladnik {
    private int id_skladnik;
    private User autor;
    private String nazwa;
    private String producent;
    private String ilosc;
    private String jednostka;
    private boolean alkohol;
    private boolean wybrany;

    public Skladnik() {
    }

    public Skladnik(String nazwa, String producent, String ilosc) {
        this.nazwa = nazwa;
        this.producent = producent;
        this.ilosc = ilosc;
    }

    public Skladnik(int id_skladnik, String ilosc, String producent, String nazwa) {
        this.id_skladnik = id_skladnik;
        this.ilosc = ilosc;
        this.producent = producent;
        this.nazwa = nazwa;
    }

    public int getId_skladnik() {
        return id_skladnik;
    }

    public void setId_skladnik(int id_skladnik) {
        this.id_skladnik = id_skladnik;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getIlosc() {
        return ilosc;
    }

    public void setIlosc(String ilosc) {
        this.ilosc = ilosc;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public String getJednostka() {
        return jednostka;
    }

    public void setJednostka(String jednostka) {
        this.jednostka = jednostka;
    }

    public boolean isAlkohol() {
        return alkohol;
    }

    public void setAlkohol(boolean alkohol) {
        this.alkohol = alkohol;
    }

    public boolean isWybrany() {
        return wybrany;
    }

    public void setWybrany(boolean wybrany) {
        this.wybrany = wybrany;
    }

    public User getAutor() {
        return autor;
    }

    public void setAutor(User autor) {
        this.autor = autor;
    }

    @Override
    public int hashCode() {
        int result = id_skladnik;
        result = 31 * result + (nazwa != null ? nazwa.hashCode() : 0);
        result = 31 * result + (producent != null ? producent.hashCode() : 0);
        result = 31 * result + (ilosc != null ? ilosc.hashCode() : 0);
        result = 31 * result + (jednostka != null ? jednostka.hashCode() : 0);
        result = 31 * result + (alkohol ? 1 : 0);
        result = 31 * result + (wybrany ? 1 : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean eq = false;
        if (obj != null && obj instanceof Skladnik)
        {
            if(this.nazwa.equalsIgnoreCase(((Skladnik) obj).nazwa)) eq = true;
        }
        return eq;
    }
}
