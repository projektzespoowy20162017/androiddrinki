package net.lubiejewski.drinkicrud;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.squareup.picasso.Picasso;

import net.lubiejewski.drinkicrud.Controller.AppController;
import net.lubiejewski.drinkicrud.Controller.Drink.DrinkFilter;
import net.lubiejewski.drinkicrud.Controller.Helpers.APIRequest;
import net.lubiejewski.drinkicrud.Controller.Skladnik.SkladnikDodaj;
import net.lubiejewski.drinkicrud.Controller.Skladnik.SkladnikListaFragment;
import net.lubiejewski.drinkicrud.Controller.User.EmailPasswordActivity;
import net.lubiejewski.drinkicrud.Controller.User.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected DrawerLayout drawer;

    private String TAG = MainActivity.class.getSimpleName();
    private FirebaseUser user;
    private String FirebaseToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //DrawerLayout
                drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //noinspection deprecation
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                View header = navigationView.getHeaderView(0);
                TextView username = (TextView) header.findViewById(R.id.drawer_username);
                TextView email = (TextView) header.findViewById(R.id.drawer_mail);
                ImageView avatar = (ImageView) header.findViewById(R.id.drawer_image);
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    user.getToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                        @Override
                        public void onComplete(@NonNull Task<GetTokenResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseToken = task.getResult().getToken();
                                Log.d(TAG, "Token: " + FirebaseToken);

                                String url = getString(R.string.serwer) + getString(R.string.table_uzytkownik) + "/" + user.getUid();
                                APIRequest request = new APIRequest(Request.Method.GET, url, FirebaseToken, new Response.Listener<NetworkResponse>() {
                                    @Override
                                    public void onResponse(NetworkResponse response) {
                                        String jsonString;
                                        try {
                                            jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                                            JSONObject jsonObject = new JSONObject(jsonString);
                                            JSONObject userData = jsonObject.getJSONObject("user");

                                            if (!userData.isNull("isAdmin")) {
                                                AppController.getInstance(getApplicationContext()).setAdmin(userData.getBoolean("isAdmin"));
                                            }

                                            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                                            if (AppController.getInstance(getApplicationContext()).isAdmin()) {
                                                navigationView.getMenu().findItem(R.id.nav_skladnik).setVisible(true);
                                                navigationView.getMenu().findItem(R.id.nav_propositionsList).setVisible(true);
                                                navigationView.getMenu().findItem(R.id.nav_propose).setVisible(false);
                                            }
                                        } catch (UnsupportedEncodingException | JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                });
                                AppController.getInstance(getApplicationContext()).addToRequestQueue(request);
                            } else {
                                FirebaseAuth.getInstance().signOut();
                            }
                        }
                    });

                    if (user.getDisplayName() != null) username.setText(user.getDisplayName());
                    else username.setText(getString(R.string.app_name));
                    if (user.getEmail() != null) email.setText(user.getEmail());
                    else email.setText(" ");
                    if (user.getPhotoUrl() != null) {
                        Picasso.with(getApplicationContext()).load(user.getPhotoUrl())
                                .fit()
                                .centerCrop()
                                .into(avatar);
                    } else {
                        //Picasso.with(getApplicationContext()).load(R.mipmap.ic_launcher).into(avatar);
                        Picasso.with(getApplicationContext()).load(android.R.color.transparent).into(avatar);
                    }

                    // User signs in
                    navigationView.getMenu().setGroupVisible(R.id.nav_zalogowany, true);
                    navigationView.getMenu().findItem(R.id.nav_shoppingList).setVisible(false);
                    navigationView.getMenu().findItem(R.id.nav_propose).setVisible(true);
                    navigationView.getMenu().setGroupVisible(R.id.nav_niezalogowany, false);
                } else {
                    // User signs out
                    navigationView.getMenu().setGroupVisible(R.id.nav_zalogowany, false);
                    navigationView.getMenu().findItem(R.id.nav_propose).setVisible(false);
                    navigationView.getMenu().setGroupVisible(R.id.nav_niezalogowany, true);
                    navigationView.getMenu().findItem(R.id.nav_skladnik).setVisible(false);
                    navigationView.getMenu().findItem(R.id.nav_propositionsList).setVisible(false);

                    username.setText(getString(R.string.app_name));
                    email.setText(getString(R.string.app_description));
                    //Picasso.with(getApplicationContext()).load(R.mipmap.ic_launcher).into(avatar);
                    Picasso.with(getApplicationContext()).load(android.R.color.transparent).into(avatar);
                }
            }
        };
        FirebaseAuth.getInstance().addAuthStateListener(authListener);
        navigationView.getMenu().setGroupVisible(R.id.nav_zalogowany, false);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.main_fragment, new DrinkFilter()).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();

        if (id == R.id.nav_drink) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_fragment, new DrinkFilter()).commit();
        } else if (id == R.id.nav_skladnik) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_fragment,new SkladnikListaFragment()).commit();
        } else if (id == R.id.nav_propositionsList) {
            //skladnikListFragment z parametrem
            Bundle propozycje = new Bundle();
            propozycje.putBoolean("mode",true);
            SkladnikListaFragment fragment = new SkladnikListaFragment();
            fragment.setArguments(propozycje);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_fragment,fragment).commit();
        } else if (id == R.id.nav_propose) {
            //skladnikDodaj z parametrem
            Intent dodaj = new Intent(this, SkladnikDodaj.class);
            dodaj.putExtra("mode",true);
            startActivityForResult(dodaj, 1);
        } else if (id == R.id.nav_zaloguj) {
            Intent logowanie = new Intent(this, EmailPasswordActivity.class);
            startActivityForResult(logowanie, 1);
        } else if(id == R.id.nav_profile){
            Intent profile = new Intent(this, UserProfile.class);
            startActivity(profile);
        } else if (id == R.id.nav_shoppingList){

        } else if (id == R.id.nav_wyloguj){
            FirebaseAuth.getInstance().signOut();
            Toast.makeText(this, getString(R.string.logged_out),
                    Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
