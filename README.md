# Szczegóły projektu zespołowego


**Temat:** Aplikacja mobilna z przepisami na drinki  
**Platforma:** Android  
**Język:** Java + wstawki   

**Składniki programu:**  

- aplikacja na smartfon z systemem Android  
- baza danych  

**Funkcje:**  

-	wyszukiwanie po rodzaju alkoholu  
-	wyszukiwanie po nazwie drinka  
-	wyszukiwanie przepisów po koszcie (opcjonalnie)  
-	rejestracja i logowanie  
-	dodawanie własnych przepisów  
-	wyliczanie kosztu jednego drinka  
-	polecanie podobnych drinków (wg. potrzebnych składników)  
-	polecanie drinków wg. historii wyszukiwań/polubień  
-	system ocen przepisów i komentarze  
-	lista najpopularniejszych przepisów  
-	lista zakupów na podstawie posiadanych składników  

**Rozwój:**  

-	generowanie aktualnej ceny drinka na podstawie cen produktów (dane pobierane z internetu, np. Ceneo API)  
-	wyświetlanie na mapie najbliższych sklepów   
-	logowanie przez facebooka, google  

**Użytkownicy:**  

1.	Użytkownik niezalogowany  

    -	może wyszukiwać i przeglądać przepisy  

2.	Użytkownik zalogowany  

    -	może wyszukiwać i przeglądać przepisy  
    -	może dodawać własne przepisy  
    -	może oceniać i komentować przepisy  

**Plan prac:**  

7.12.2016 - rekonesans podobnych aplikacji, diagram użycia, struktura bazy danych - diagram ERD  
21.12.2016 - poprawić diagram użycia i ERD, zrobić bazę danych, CRUD